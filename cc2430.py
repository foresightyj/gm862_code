import SER
import sys
import MDM
import GPIO

#twochars2onechar = lambda twochars: chr((ord(twochars[0])<< 4) | (0x0f & ord(twochars[1])))

class CC2430:
	def __init__(self):
		#Set Serial Port Speed
		#SER.set_speed('115200','8N1')
		
		# A table that has correspondance of NWK and IEEE.
		# CC2430 module keeps this info.
		# Elderly module talks to CC2430 module via IEEE addr.
		# CC2430 module figures out the NWK addr and talks to CC2430 hardware using that NWK addr.
		
		self.CC2430ENABLED = 1

		#MSG_ID definitions. MUST be the same as defined in CC2430.
		self.PING_MSG_ID						= 0
		self.TEMP_REPORT_MSG_ID 				= 1
		self.BATT_REPORT_MSG_ID 				= 2
		self.SOS_ALERT_MSG_ID 					= 3
		self.ACCEL_REPORT_MSG_ID 				= 4
		self.STATUS_RSP_MSG_ID 					= 5
		self.STATUS_REQ_MSG_ID 					= 6
		self.REMINDER_MSG_ID 					= 7
		self.CONTROLLED_IEEE_REQ_MSG_ID 		= 8
		self.CC2430_DEBUG_MSG_ID				= 9
		self.IEEE_NWK_UPDATE_MSG_ID				= 0x0a	#0x0a
		self.NWK_ADDR_REQ_MSG_ID				= 0x0b	#0x0b
		self.IEEE_ADDR_REQ_MSG_ID				= 0x0c	#0x0c
		self.LOCATION_LINK_UPDATE_MSG_ID		= 0x0d	#0x0d
		self.ROUTER_CHECK_COORD_MSG_ID			= 0x10
		self.ROUTER_DELETE_CHILD_MSG_ID 		= 0x11
		self.NON_CHILD_REPORT_MSG_ID			= 0x12
		self.WHAT_IS_YOUR_IEEE_MSG_ID			= 0x13 #IEEE_ADDR_REQ does not reach End Device reliably
		
	def run_CC2430(self):
		# Notify CC2430 so it can continue to run the program
		if self.CC2430ENABLED:
			#GPIO.setIOdir(8, 1, 1) # For PCB Version 2.
			#GPIO.setIOvalue(8, 1)
			GPIO.setIOdir(13, 1, 1) # For PCB Version 3. connected to P2_1_DD in CC2430.
			GPIO.setIOvalue(13, 1)
	
	
	def PING(self, timeout):
		to = 0
		
		while not self.__PING():
			to = to + 1
			if to > timeout:
				return 0
		
		return 1
	
	def __PING(self):
		tobesent=chr(2)+ chr(self.PING_MSG_ID) + chr(0xff)
		expect = chr(1)+ chr(self.PING_MSG_ID)
		#res = SER.receive(2) # clear stuff that exists in the UART buffer before pinging.
		#no need. just let the first PING fail, the second will survive.
		
		SER.send(tobesent)
		cc2430 = SER.receive(5)
		
		timeout = 0
		while cc2430.find(expect) < 0:
			timeout = timeout + 1
			if timeout > 10:
				return 0
			
			cc2430 = cc2430 + SER.receive(2)
			
		return 1
		
	def sendMsg2CC2430(self, MSG_ID, Payload):
		# LEN (1 byte) + MSG_ID (1 byte) + Payload
		tobesent=chr(len(Payload)+2)+ chr(MSG_ID) + Payload+chr(0xff)
		SER.send(tobesent)
		return tobesent
	
	def receiveMsgFromCC2430(self):
		#This function needs serious optimization!!!!!
		
		# LEN (1 byte) + MSG_ID (1 byte) + Payload
		try:
			CC2430MsgBuffer = []
			res = SER.receive(1) #originally 10
			cc2430 = res
			while res != '':
				res = SER.receive(1) #originally 5
				cc2430 = cc2430 + res
			
			index = 0
			totallen = len(cc2430)
			while index < totallen:
					end = index+ord(cc2430[index])+1
					CC2430MsgBuffer.append((ord(cc2430[index+1]), cc2430[index+2:end]))
					index = end
			
		except:
			#unknown exception
			#MyPrinter('Exception occurred.\r\n')
			exc_type, exc_value, exc_traceback = sys.exc_info()
			totallen = -1 #indicating error.
			CC2430MsgBuffer.append((self.CC2430_DEBUG_MSG_ID, 'RECEIVEERROR: ' + str(exc_type) + ', ' + str(exc_value)))
			return CC2430MsgBuffer, totallen
		
		return CC2430MsgBuffer, totallen
