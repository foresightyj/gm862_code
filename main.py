#@author Yuan Jian
import MDM	#accessing AT command interface
import MOD	#for timers
import SER
import sys

SER.set_speed('115200','8N1')
SER.send('Starting e-Guardian.\r\n')

from system import System

class SERWriter:
    def write(self,s):
        SER.send(s+'\r')
sys.stdout = sys.stderr = SERWriter()

#Initialize Elderly Monitoring System
system = System()

#Initialize Watchdog Timer
MOD.watchdogEnable(60*5) #watchdog timer == always 2 minutes longer than the sleep time

SER.send('System initialized.\r\n')

def MyPrinter(msg):
	global system
	if sys.modules.has_key('SER') and system.shell.SERIALENABLED:
		SER.send(msg)
		pass
	elif system.shell.socket:
		system.shell.socket.sendGprsMsg(msg)
		
MyPrinter('\r\nSTART!\r\n')

system.cc2430.run_CC2430() #release the reset pin of CC2430 to let it run.
MOD.sleep(20)

# Start System Main Loop
if system.cc2430.CC2430ENABLED:
	if system.cc2430.PING(3):
		MyPrinter('PING!\r\n')
	else:
		MyPrinter('PING FAILED! Pls reset CC2430.\r\n')
		MOD.sleep(15)
		if system.cc2430.PING(10):
			MyPrinter('PING!\r\n')
		else:
			MyPrinter('PING FAILED without retry!\r\n')


round = 0L
#MyPrinter('\r\nHERE!\r\n')
while 1: #round < 100:
	
	round = round + 1
	#MyPrinter('R'+str(round))
	currentTime = MOD.secCounter()

	################################################################################################
	################################### Check CC2430 Slave Message #################################
	################################################################################################
	if system.cc2430.CC2430ENABLED:
		try:
			parsedCC2430Msgs = system.parseCC2430Msgs()
			#a = MOD.secCounter()-currentTime
			# notify appropriate listeners
			system.processCC2430Msgs(parsedCC2430Msgs, currentTime) 
			#b = MOD.secCounter()-currentTime
			# print it out
			#for parsedCC2430Msg in parsedCC2430Msgs:
			#	if parsedCC2430Msg[0] in ('ANNCE', 'DEBUG'):
			#		MyPrinter(str(parsedCC2430Msg)+'\r\n')

		except:
			MyPrinter(system.printStackTrace('Weird Exception:\r\n'))
			#raise
			
	################################################################################################
	##################################### Process System Events ####################################
	################################################################################################
	system.processSystemEvents(currentTime) #processCC2430Msgs and processSystemEvents must be passed into the same currentTime
	
	#c = MOD.secCounter()-currentTime

	################################################################################################
	########################################## Process Commands ####################################
	################################################################################################
	#shell commands with least priority so processed in the last step.
	orders = system.shell.processShell()
	
	#d = MOD.secCounter()-currentTime
	#if round%50==0:
	#		MyPrinter('%d %d %d %d' % (a,b,c,d))
	MOD.watchdogReset()
