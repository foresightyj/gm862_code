import MDM
import MOD

class Time:
	def __init__(self, system):
		##SER.send('Setting up time using NITZ...\r\n')
		self.system = system
		
		#cache database
		self.mydatabase = self.system.database
		self.prevTime = 86400
		res = MDM.send ('AT#NITZ?\r', 0)
		res = MDM.receive(10)
		if res.find("7,0") < 0 :
			#Set up Nitz
			res = MDM.send ('AT#NITZ=7,0\r', 0)
			res = MDM.receive(5)
			##SER.send(res)
			res = MDM.send ('AT&P0\r', 0)
			res = MDM.receive(5)
			##SER.send(res)
			res = MDM.send ('AT&W0\r', 0)
			res = MDM.receive(5)
			##SER.send(res)
			
			#Reboot system
			##SER.send('Reboot for NITZ to take effect ...\r\n')
			MOD.sleep(2)
			MDM.send('AT#REBOOT\r',2)
			res = MDM.receive(20)
			while(res.find('OK')<0):
				res = MDM.send('AT#REBOOT\r',0)
				##SER.send('Keep rebooting until success.\r\n')
				res = MDM.receive(20)
		else:
			res = MDM.send ('AT#CCLK?\r', 0)
			res = MDM.receive(10)
			##SER.send(res)
			timeout = 0 # Some SIM does not support nitz so it has to give up.
			while (res.find("#CCLK") == -1 or res.find("00/01/01") > -1) and timeout < 10:
				res = MDM.send ('AT#CCLK?\r', 0)
				res = MDM.receive(10)
				timeout = timeout + 1
				##SER.send(res)
			
			if timeout >= 10:
				#get time from server
				self.syncTimeReq()
			#System clock is successfully initialized in the network time.
	
	def syncTimeReq(self):
		if self.system.shell.socket:
			self.system.shell.sendMsg('gprs', 'syncTimeReq\r\n')
			
	def syncTimeRsp(self, timestr):
		#time = (int(timestr[0:2]),int(timestr[2:4]),int(timestr[4:6]),int(timestr[6:8]),int(timestr[8:10]),int(timestr[10:12]))
		res = MDM.send ('AT+CCLK = "' + timestr + '+00"\r', 0)
		res = MDM.receive(2)
		while res.find('OK') < 0:
			res = MDM.receive(2)
	
	def getTime(self): #Simple implementation, return in string. Improve later.
		res = MDM.send ('AT#CCLK?\r', 0)
		res = MDM.receive(2)
		while(res.find('#CCLK:')<0):
			res = MDM.receive(2)
		
		start = res.find('#CCLK: "')
		end = res.find('+')

		i = res.find('/')
		
		return (int(res[i-2:i]), int(res[i+1:i+3]), int(res[i+4:i+6]), int(res[i+7:i+9]), int(res[i+10:i+12]), int(res[i+13:i+15]))

	def checkReminder(self, currTime):
		#pull out all reminders
		#return a list of ieee numbers that is due to alert

		ieeelist = []
		reminderlist = self.system.database.getReminderList()
		#self.system.shell.sendMsg('gprs', str(reminderlist))
		currTime = currTime % 86400
		for ieee, alertTime in reminderlist:
			#alertTime is modulo of 24*60*60 = 86400
			
			if alertTime <= currTime and self.prevTime < alertTime:
				#print self.prevTime, currTime, fullAlertTime
				#print 'YES!!!!!!!!!!!!!'
				ieeelist.append(ieee)
				#self.system.shell.sendMsg('gprs', str(ieeelist))
		
		self.prevTime = currTime
		return ieeelist
		