#!/usr/bin/python2.7
import marshal

#define DataBase
FACTORY_ADMINACCOUNT = {'pwd': '12345', 'missingTrigger': 15, 'smsEnable': 1} # dictionary

#Permit Level Definition
ADMIN_LEVEL = 2
USER_LEVEL = 1
NON_USER_LEVEL = 0

ALL_SUCCESS = 0
IEEE_SUCCESS = 1
PHONE_SUCCESS = 2
UNAUTHROIZED = 3
INVALID = 4

class Database:
        #Method Convention
        # prefix '__':                  private methods
        # CAPITAL_LETTERS:              methods only used by the system, not by Shell, thus permitLevel not needed.
        # others:                               methods used by SMS commands.
        def __init__(self):
                ###SER.send('Initialize Database!\r\n')
                self.adminaccount = FACTORY_ADMINACCOUNT
                self.gprssetting = ()
                self.devicelist = {}
                self.reminderlist = {}
                self.ieeealiaslist = {}
                self.phonealiaslist = {}
                self.routeraliaslist = {}
                
                if self.__checkDbInRomIntegrity():
                        #Restore settings from database.
                        for attr in self.__dict__.keys(): #restore all attributes.
                                self.__restoreFromDbInRom(attr)
                else: 
                        # Initialize using Factory Defaults
                        self.format2Factory()
                ##SER.send('Database Initialized.\r\n')
        
        def getAdmin(self):
                return self.adminaccount['admin']
                
        def getCaregiversByIEEE(self, ieeeHex):
                return self.devicelist.get(ieeeHex,'') #more efficient
        
        def getAllIEEEByPhone(self, phoneNo):
                phoneNo = phoneNo[-8:]
                return map(lambda ieeeAndCaregivers: ieeeAndCaregivers[0], filter(lambda ieeeAndCaregivers, phoneNo = phoneNo: phoneNo in ieeeAndCaregivers[1], self.devicelist.items()))

        def getAllPhones(self):
                allCaregivers = self.devicelist.values()
                allCaregivers = self.__flattenListofLists(allCaregivers)
                return self.__removeDuplicates(allCaregivers)
        
        def getIEEEAndPhonesByDevId(self, devidHex, cacheTable={}):
                try:
                        if cacheTable.has_key(devidHex):
                                ieeeHex = cacheTable[devidHex]
                                return (ieeeHex, self.devicelist[ieeeHex])
                        else:
                                for ieeeHex, caregivers in self.devicelist.items():
                                        if ieeeHex.endswith(devidHex):
                                                cacheTable[devidHex]=ieeeHex
                                                return (ieeeHex, caregivers)
                                return ()
                except KeyError:
                        #while a ieeeHex is stored in cacheTable, it might be deleted from self.devicelist after a while
                        #In this case KeyError will occur, we have to remove this map from cacheTable
                        if cacheTable.has_key(devidHex):
                                del cacheTable[devidHex]
                        return ()
                        
        def checkPassword(self, somepwd):
                return somepwd == self.adminaccount['pwd']
               
        def getIEEEAlias(self, ieeeHex):
                return self.ieeealiaslist.get(ieeeHex, '')

        def replaceByIEEEAlias(self, ieeeHex,cachethis=0):
                return self.ieeealiaslist.get(ieeeHex,self.Hex2Str(ieeeHex,cachethis=cachethis))
        
        def getOriginalIEEE(self,ieeeAlias):
                for origin, alias in self.ieeealiaslist.items():
                        if alias == ieeeAlias:
                                return origin
                return ''
                
        def replaceByOriginalIEEE(self, ieeeAlias):
                for origin, alias in self.ieeealiaslist.items():
                        if alias == ieeeAlias:
                                return origin
                #otherwise
                return ieeeAlias
                        
        def getPhoneAlias(self, phoneNo):
                return self.phonealiaslist.get(phoneNo,'')
                
        def replaceByPhoneAlias(self, phoneNo):
                return self.phonealiaslist.get(phoneNo,phoneNo)

        def getOriginalPhone(self, phoneAlias):
                for origin, alias in self.phonealiaslist.items():
                        if alias == phoneAlias:
                                return origin
                return ''
        
        def replaceByOriginalPhone(self, phoneAlias):
                for origin, alias in self.phonealiaslist.items():
                        if alias == phoneAlias:
                                return origin
                return phoneAlias

        def replaceRouterAlias(self, routerHex):
                #'''
                #Give router's ieee address in hex, give its alias or its str representation.
                #'''
                return self.routeraliaslist.get(routerHex, self.Hex2Str(routerHex,cachethis=1))
                
        def recognize(self, phoneNoOrId, disablePhone = 0, nonAdmin = ''):
                #'''
                #This function takes IEEE addresses, Dev IDs, phone numers and aliases for IEEE address or phone numbers
                #and produces ieeeHex or phone numbers.
                #
                #If this function is invoked by admin, phoneNoOrId can be any of the five possibilities:
                #        (1)IEEE, (2)DEVID, (3)phone no, (4)alias for IEEE, (5)alias for phone no.
                #
                #       phoneNoOrId can be one of the following five:
                #            2726252423220009, 2423220009, 96368893, old3, yuan_jian
                #        phone no is more tricky, it can be: 96368893, 6596368893, +6596368893, 006596368893
                #
                #If this function is invoked by non-admin user, phoneNoOrId can only be three possibilities as follows:
                #        (1)IEEE, (2)DEVID, (3)alias for IEEE.
                #'''
                
                all_phone_numbers = ''
                
                #check if it is an alias for IEEE or phone no.
                ieeeHex = self.getOriginalIEEE(phoneNoOrId)
                phoneNo = ''
                if not ieeeHex and not nonAdmin:
                        phoneNo = self.getOriginalPhone(phoneNoOrId)
                
                if not ieeeHex and not phoneNo:
                        # Remaining three cases: 2726252423220009, 2423220009, or
                        # 96368893, 6596368893, +6596368893, 006596368893

                        #check if it is dev id
                        devId = self.validateHexStr(phoneNoOrId,10)
                        if devId:
                                #check if this dev id corresponds to an IEEE addr
                                ieeePhoneNoPair = self.getIEEEAndPhonesByDevId(self.Str2Hex(devId)) # phoneNoOrId is a devId
                                if ieeePhoneNoPair:
                                        ieeeHex = ieeePhoneNoPair[0]
                        if not ieeeHex:
                                # Remaining two cases: 2726252423220009, or
                                # 96368893, 6596368893, +6596368893, 006596368893

                                #check if it is an ieee addr
                                #Do NOT check if ieeeHex is in the devicelist
                                hex16 = self.validateHexStr(phoneNoOrId)
                                if hex16:
                                        ieeeHex = self.Str2Hex(hex16)
                        if not ieeeHex and not nonAdmin:
                                # Only 1 case left:
                                # 96368893, 6596368893, +6596368893, 006596368893
                                if not disablePhone and self.isValidPhoneNo(phoneNoOrId):
                                        #check if its last 8 digits constitutes a phone no in devicelist
                                        if all_phone_numbers == '':
                                                # cached calculation
                                                all_phone_numbers = self.getAllPhones()
                                        if phoneNoOrId[-8:] in all_phone_numbers:
                                                phoneNo = phoneNoOrId[-8:]

                
                #ieeeHex and phoneNo can not be non-empty at the same time
                return (ieeeHex, phoneNo)

        def updateAlias(self, aliasLists, permitLevel, commit=1):
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can change alias.\r\n'
                
                #aliasLists is something like [['00ffffffff','old1'], ['98989898','yuan_jian'], ['yuan_jian','yuanjian']]
                
                #for alias in aliasLists:
                #        alias[0]= self.replaceByOriginalIEEE(alias[0])
                #        alias[0]= self.replaceByOriginalPhone(alias[0])
                
                #isUpper = lambda U: U>='A' and U<='Z'
                #isLower = lambda l: l>='a' and l<='z'
                #isDIGIT = lambda d: d >='0' and d <='9'
                
                #BELOW DOES NOT WORK in Python 1.5.2, but it will work in PYTHON 2.7
                #isAllDigits = lambda str1: str1 and filter(isDIGIT, str1) == str1
                
                #CORRECT FORM BELOW:
                #REASON: in early version Python (before version 2.1), 
                #scope of enclosing functions (which applies to both def and lambda) is not seen by inner functions.
                #A default argument can solve the problem, as shown below:
                #isAllDigits = lambda str1, isDIGIT = isDIGIT: str1 and filter(isDIGIT, str1) == str1
        
                isAllDigits = lambda str1: str1 and filter(lambda s : s >='0' and s <='9', str1) == str1 #str1 must not be empty
                
                #isValidAlias = lambda name: name and filter(lambda c: isDIGIT(c) or isUpper(c) or isLower(c) or isOther(c), name) == name
                #A valid alias should not be empty; should start with a non-digit character and only contains alphameric and '_'
                isValidAlias = lambda alias: alias and (alias[0] <'0' or alias[0] >'9') and\
                filter(lambda s: (s >= '0' and s<='9') or (s >= 'a' and s<='z') or (s >= 'A' and s<='Z') or s == '_', alias) == alias


                isQuote = lambda c: c == '"' or c == "'"
                
                reply_msg = ''
                updateieeealias = 0
                updatephonealias = 0
                for phoneNoOrId, alias in aliasLists:
                        
                        this_reply = ''
                        #strip ' and " around alias if any
                        if alias and isQuote(alias[0]) and isQuote(alias[-1]):
                                alias = alias[1:-1]

                        if isValidAlias(alias) or alias == '':
                                ieeeHex, phoneNo = self.recognize(phoneNoOrId)
                                if ieeeHex:
                                        if alias: # add
                                                #check if this alias name is already used to avoid duplicated alias names.
                                                if not self.getOriginalIEEE(alias): #available
                                                        if self.devicelist.has_key(ieeeHex):
                                                                self.ieeealiaslist[ieeeHex] = alias
                                                                updateieeealias = 1
                                                                this_reply = 'Device %s is aliased to %s.\r\n' % (phoneNoOrId, alias)
                                                        else:
                                                                this_reply = 'No device %s in system.\r\n' % phoneNoOrId
                                                else:
                                                        this_reply = 'Alias name %s has been used.\r\n' % alias
                                        else: #delete
                                                if self.ieeealiaslist.has_key(ieeeHex):
                                                        del self.ieeealiaslist[ieeeHex]
                                                        updateieeealias = 1
                                                        this_reply = "Device %s's alias is removed\r\n" % phoneNoOrId
                                                else:
                                                        this_reply = 'No existing alias for %s.\r\n' %phoneNoOrId
                                elif phoneNo:
                                        if alias: # add
                                                if not self.getOriginalIEEE(alias): #available
                                                        self.phonealiaslist[phoneNo] = alias
                                                        updatephonealias = 1
                                                        this_reply = 'Caregiver %s is aliased to %s.\r\n' % (phoneNoOrId, alias)
                                                else:
                                                        this_reply = 'Alias name %s has been used.\r\n' % alias
                                        else: #delete
                                                if self.phonealiaslist.has_key(phoneNo):
                                                        del self.phonealiaslist[phoneNo]
                                                        updatephonealias = 1
                                                        this_reply = "Caregiver %s's alias is removed\r\n" % phoneNoOrId
                                                else:
                                                        this_reply = 'No existing alias for %s.\r\n' %phoneNoOrId
                                else:
                                        this_reply = '%s is neither a device nor a phone no\r\n' %phoneNoOrId
                                
                        else:
                                #invalid alias
                                this_reply = "%s is an invalid alias.\r\n" % alias
                                
                        reply_msg = reply_msg + this_reply
                        
                if commit:
                        if updateieeealias: self.__updateDbInRom('ieeealiaslist')
                        if updatephonealias: self.__updateDbInRom('phonealiaslist')
                return reply_msg
        
        def addRouters(self, routerAndAliasList, permitLevel):
                #'''
                #Adds routers and their aliases at one go.
                #Routers' aliases could be very loose, numbers are also ok.
                #Actually they are not validated at all.
                #'''
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can add routers and their aliases.\r\n'
                
                reply_msg = ''
                for router, alias in routerAndAliasList:
                        hex16 = self.validateHexStr(router)
                        if hex16:
                                ieeeHex = self.Str2Hex(hex16)
                                if alias not in ("''", '""'): #add
                                        self.routeraliaslist[ieeeHex] = alias
                                        this_reply = 'Router %s added and aliased to %s.\r\n' % (hex16, alias)
                                else: #delete
                                        if self.routeraliaslist.has_key(ieeeHex):
                                                this_reply = 'Router %s removed.\r\n' % hex16
                                                del self.routeraliaslist[ieeeHex]
                                        else:
                                                this_reply = 'Router %s does not exist.\r\n' % hex16
                        else:
                                this_reply = 'Invalid IEEE address for routers.\r\n'
                                
                        reply_msg = reply_msg + this_reply
                
                self.__updateDbInRom('routeraliaslist')
                
                return reply_msg
        
        def store(self, number, ieeelist, permitLevel):
                #number = self.replaceByOriginalIfExist(number)
                #ieeelist = map(self.replaceByOriginalIfExist, ieeelist)
                
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can add new users.\r\n'
                
                if len(ieeelist) == 0:
                        return 'Please specify a list of devices.\r\n'
                        
                #check if this ieee == valid
                reply_msg = ''
                
                number2 = self.getOriginalPhone(number)
                if not number2:
                        if not self.isValidPhoneNo(number):
                                return number + ' is not a known phone alias to the system.\r\n'
                        else:
                                number2 = number
                
                updatedevicelist = 0
                
                for ieee in ieeelist:
                        ieeeHex = self.recognize(ieee, disablePhone = 1)[0]
                        
                        if not ieeeHex:
                                reply_msg = reply_msg + '%s is an invalid IEEE address.\r\n' % ieee
                        else:
                                #check if this ieee exists
                                if self.devicelist.has_key(ieeeHex): #already has at least 1 caregiver
                                        #check if it is already there
                                        if number2[-8:] in self.devicelist[ieeeHex]:
                                                reply_msg = reply_msg + 'Device: ' + ieee +' was already associated with ' + number + '.\r\n'
                                        else:
                                                self.devicelist[ieeeHex].append(number2[-8:])
                                                updatedevicelist = 1
                                                reply_msg = reply_msg + 'Device: ' + ieee +' is associated with ' + number + '.\r\n'
                                else: #these is no caregiver at the moment
                                        self.devicelist[ieeeHex] = [number2[-8:]] #only store the last 8 digits of a phone number
                                        updatedevicelist = 1
                                        reply_msg = reply_msg + 'Device: ' + ieee +' is associated with ' + number + '.\r\n'

                
                if updatedevicelist: 
                        self.__updateDbInRom('devicelist')
                
                return reply_msg
                        
                
        def delete(self, ieeeOrPhoneNoOrAliasList, permitLevel):
                #'''
                #This function accepts IEEE addresses, Dev IDs, phone numers and aliases for IEEE address or phone numbers
                #'''
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can delete users.\r\n'
                
                if len(ieeeOrPhoneNoOrAliasList) == 0:
                        return 'Please specify a phone no or a list of devices.\r\n'
                
                #check if argv is a phone no
                updatedevicelist = 0
                updatereminderlist = 0
                updateieeealiaslist = 0
                updatephonealiaslist = 0
                
                reply_msg = ''
                for ieeeOrPhoneNoOrAlias in ieeeOrPhoneNoOrAliasList:
                        ieeeHex, phoneNo = self.recognize(ieeeOrPhoneNoOrAlias)

                        #self.updateAlias([[ieeeOrPhoneNoList[0], '']], ADMIN_LEVEL, commit=0)
                        this_reply = ''
                        if ieeeHex:
                                #only a single IEEE address
                                #delete this device and also check if any of its caregiver has only 1 device (which is this)
                                if self.devicelist.has_key(ieeeHex):
                                        del self.devicelist[ieeeHex]
                                        updatedevicelist = 1
                                        #meanwhile reminders associated with this ieee should also be deleted
                                        if self.reminderlist.has_key(ieeeHex):
                                                del self.reminderlist[ieeeHex]
                                                updatereminderlist = 1
                                        
                                        #meanwhile delete their aliases
                                        this_reply = this_reply + 'Device %s is deleted.\r\n' % self.replaceByIEEEAlias(ieeeHex)

                                        #delete alias after showing alias in the reply_msg
                                        if self.ieeealiaslist.has_key(ieeeHex):
                                                del self.ieeealiaslist[ieeeHex]
                                                updateieeealiaslist = 1
                                else:
                                        this_reply = this_reply + '%s is neither an authorized phone no nor an existing device.\r\n' % ieeeOrPhoneNoOrAlias
                                
                        elif phoneNo:
                                #a list of devices belonging to one phoneNo.
                                myIeeeHexs = self.getAllIEEEByPhone(phoneNo)
                                for ieeeHex in myIeeeHexs:
                                        caregivers = self.devicelist[ieeeHex]
                                        if len(caregivers)==1:
                                                #delete this IEEE
                                                this_reply = this_reply + 'Device %s associated with %s is deleted.\r\n' % (self.replaceByIEEEAlias(ieeeHex),ieeeOrPhoneNoOrAlias) 
                                                del self.devicelist[ieeeHex]
                                                updatedevicelist = 1
                                                #meanwhile reminders associated with this ieee should also be deleted
                                                if self.reminderlist.has_key(ieeeHex):
                                                        del self.reminderlist[ieeeHex]
                                                        updatereminderlist = 1

                                                #delete its alias if exists
                                                if self.ieeealiaslist.has_key(ieeeHex):
                                                        del self.ieeealiaslist[ieeeHex]
                                                        updateieeealiaslist = 1
                                        else:
                                                #just remove this phoneNo from caregivers
                                                this_reply = this_reply + "Caregiver %s is removed from %s's caregiver list\r\n" % (ieeeOrPhoneNoOrAlias,self.replaceByIEEEAlias(ieeeHex))
                                                caregivers.remove(phoneNo)
                                                updatedevicelist = 1
                                
                        else:
                                this_reply = this_reply + '%s is neither an authorized phone no nor an existing device\r\n' % ieeeOrPhoneNoOrAlias

                        reply_msg = reply_msg + this_reply
                        
                #check if any caregiver in phonealiaslist does not have IEEE attached to him/her

                allCaregivers = self.getAllPhones()
                for caregiver, alias in self.phonealiaslist.items():
                        
                        #check if this caregiver has an alias
                        if caregiver not in allCaregivers:
                                #remove this caregiver's alias
                                #silient removal
                                if self.phonealiaslist.has_key(caregiver):
                                        del self.phonealiaslist[caregiver]
                                updatephonealiaslist = 1
                
                #avoid unnecessarily update flash
                if updatedevicelist: self.__updateDbInRom('devicelist')
                if updatereminderlist: self.__updateDbInRom('reminderlist')
                if updateieeealiaslist: self.__updateDbInRom('ieeealiaslist')
                if updatephonealiaslist: self.__updateDbInRom('phonealiaslist')
        
                return reply_msg

                
        
        def actOnTargets(self, ieeeOrPhoneNoList, actionFunc, inquirer, permitLevel):
                #'''
                #make sure permitLevel is either USER_LEVEL or ADMIN_LEVEL
                #actionFunc takes a single ieeeHex and act upon it.
                #Optionally actFunc could return ieeeHex if actionFunc failed on that ieeeHex
                #'''
                
                status = []
                failed = []
                for ieeeOrPhoneNo in ieeeOrPhoneNoList:
                        
                        if ieeeOrPhoneNo.lower() == 'all':
                                if permitLevel is USER_LEVEL:
                                        #apply it to all its IEEEs
                                        alldevices = self.getAllIEEEByPhone(inquirer[-8:])
                                else:
                                        #apply to all devices in the system
                                        alldevices = self.devicelist.keys()

                                #this_reply = this_reply + ','.join(map(self.replaceByIEEEAlias, alldevices))
                        
                                for ieeeHex in alldevices:
                                        if actionFunc(ieeeHex):
                                                failed.append(ieeeHex)
                                
                                status.append(ALL_SUCCESS)

                        else:
                                if permitLevel is USER_LEVEL:
                                        ieeeHex= self.recognize(ieeeOrPhoneNo, nonAdminNo = 1)[0]
                                        if ieeeHex:
                                                #check if this user can access this ieeeHex
                                                if inquirer[-8:] in self.devicelist[ieeeHex]:
                                                        if actionFunc(ieeeHex):
                                                                failed.append(ieeeHex)
                                                        
                                                        status.append(IEEE_SUCCESS)
                                                else:
                                                        status.append(UNAUTHROIZED)
                                        else:
                                                status.append(INVALID)
                                        
                                else:
                                        ieeeHex, phoneNo = self.recognize(ieeeOrPhoneNo)
                                        if ieeeHex:
                                                if actionFunc(ieeeHex):
                                                        failed.append(ieeeHex)
                                                status.append(IEEE_SUCCESS)
                                        elif phoneNo:
                                                allIeeeHexs = self.getAllIEEEByPhone(phoneNo)
                                                for ieeeHex in allIeeeHexs:
                                                        if actionFunc(ieeeHex):
                                                                failed.append(ieeeHex)
                                                status.append(PHONE_SUCCESS)
                                        else:
                                                status.append(INVALID)
                return status, failed
            
        def reminder(self, hhmmss, ieeeOrPhoneNoList, inquirer, permitLevel):
                if permitLevel < USER_LEVEL:
                        return 'You are not authorized to place a reminder.\r\n'
                
                if hhmmss: #add reminder
                        atype = 'added' #action type
                        def action(ieeeHex, hhmmss=hhmmss, list = self.reminderlist): #python 1.5.4 workaround.
                                list[ieeeHex] = hhmmss
                else: #delete reminder
                        atype = 'deleted'
                        def action(ieeeHex, hhmmss=hhmmss, list = self.reminderlist): #python 1.5.4 workaround.
                                if list.has_key(ieeeHex):
                                        del list[ieeeHex]
                        
                status = self.actOnTargets(ieeeOrPhoneNoList, action, inquirer, permitLevel)[0]
                 
                reply_msg = ''
                for i in range(len(ieeeOrPhoneNoList)):
                        if status[i] == ALL_SUCCESS:
                                if permitLevel == ADMIN_LEVEL:
                                        this_reply = 'Reminders for all devices %s.\r\n' % atype
                                else:
                                        this_reply = 'Reminders for all devices associated with %s %s.\r\n' % (self.replaceByPhoneAlias(inquirer[-8:]), atype)
                        elif status[i] == IEEE_SUCCESS:
                                this_reply = 'Reminders for %s %s.\r\n' % (ieeeOrPhoneNoList[i],atype)
                        elif status[i] == PHONE_SUCCESS:
                                this_reply = 'Reminders for all devices associated with %s %s.\r\n' % (ieeeOrPhoneNoList[i],atype)
                        elif status[i] == UNAUTHROIZED:
                                this_reply = 'You are unauothorized to access %s.\r\n' % ieeeOrPhoneNoList[i]
                        elif status[i] == INVALID:
                                this_reply = 'Unknown device or phone no: %s' % ieeeOrPhoneNoList[i]
                        
                        reply_msg = reply_msg + this_reply

                if IEEE_SUCCESS in status or PHONE_SUCCESS in status or ALL_SUCCESS in status:
                        self.__updateDbInRom('reminderlist')
                return reply_msg

        def getGprsSetting(self):
                #return a tuple of(APN, USERID, PASSW, IP, PORT_NO)
                #if can not find, return an empty tuple
                return self.gprssetting

                
        def updateGprsSetting(self, gprsTuple, permitLevel, commit=1):
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can change GPRS settings.\r\n'
                if len(gprsTuple) != 5:
                        return 'Wrong GPRS setting length.\r\n'
                self.gprssetting = gprsTuple

                if commit:
                        self.__updateDbInRom('gprssetting')
                return 'Updating GPRS setting is done.\r\n'
        
        def changePassword(self, new_pwd, permitLevel):
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can change the password.\r\n'
                self.adminaccount['pwd'] = new_pwd
                self.__updateDbInRom('adminaccount')
                return 'Changing password is done.\r\n'
                
        def changeAdmin(self, new_number, permitLevel):
                if permitLevel != ADMIN_LEVEL:
                        return 'Only admin can change admin phone no.\r\n'
                self.adminaccount['admin'] = new_number
                self.__updateDbInRom('adminaccount')
                return 'Changing admin phone no is done.\r\n'
        
        def updateTrigger(self, trigger, updateCb):
                if trigger >= 6 and trigger <= 600:
                        self.adminaccount['missingTrigger'] = trigger
                        updateCb(trigger)
                        self.__updateDbInRom('adminaccount')
                        return 'Updating trigger done.\r\n'
                else:
                        return 'Make sure trigger value is between 6 and 600.\r\n'
                
        def getTrigger(self):
                return self.adminaccount.get('missingTrigger', 15)
        
        def updateSmsEnable(self, enable, updateCb): #enable is 0 or 1
                if enable == 'on':
                    self.adminaccount['smsEnable'] = 1
                    reply_msg =  'sms is on.\r\n'
                elif enable == 'off':
                    self.adminaccount['smsEnable'] = 0
                    reply_msg =  'sms is off.\r\n'
                else:
                    return "Key in either 'sms on' or 'sms off'.\r\n"
                
                updateCb(self.adminaccount['smsEnable'])
                self.__updateDbInRom('adminaccount')
                return reply_msg
        
        def getSmsEnable(self):
                return self.adminaccount.get('smsEnable', 0) #default: sms disabled.
                
        def validateHexStr(self, hexStr, length=16):
                hexStr = hexStr.lower()
                if len(hexStr) == length:
                        isLowerHex = lambda str1: filter(lambda s : (s >='0' and s <='9') or (s >='a' and s <='f'), str1) == str1
                        if isLowerHex(hexStr):
                                return hexStr
                return ''
                
        def isValidPhoneNo(self, phoneNo):
                #a valid phone number should only contain '+', '-' and digits
                #first '+' and '-' are limited to appear only once.
                #and the number of digits should be not less than 8
                temp = phoneNo.replace('+', '')
                if len(phoneNo) - len(temp) > 1:
                        return 0
                temp = temp.replace('-','')
                if len(temp) - len(temp) > 1:
                        return 0
                
                return filter(lambda d: d >='0' and d<='9', temp) == temp and len(temp) >= 8
                
        
        def getReminderList(self):
                return self.reminderlist.items()
                
        def format2Factory(self, admin_no=''):
                self.adminaccount = FACTORY_ADMINACCOUNT
                self.gprssetting = ()
                self.devicelist = {}
                self.reminderlist = {}
                self.phonealiaslist = {}
                self.ieeealiaslist = {}
                
                if admin_no != '':
                        self.adminaccount['admin'] = admin_no
                for attr in self.__dict__.keys(): #restore all attributes.
                        self.__updateDbInRom(attr)
                        
                return 'Format successful.\r\n'

        def __checkDbInRomIntegrity(self): #dumb function first. May be implemented when I have time.
                try:
                        
                        for db in self.__dict__.keys():
                                f = open('_'+db.upper(),'rb') #filenames are just '_' + dictionary_names.
                                stored = marshal.load(f)
                                f.close()
                        return 1
                except IOError:
                        ##SER.send('Opening the file failed.\r\n')
                        return 0
                except EOFError:
                        print 'EOF detected.\r\n'
                except:
                        print 'Error experienced when validating stored system info.\r\n'

        def __restoreFromDbInRom(self, attr): # assuming the stored sysInfo == valid.
                try:
                        f = open('_'+attr.upper(),'rb')
                        t = marshal.load(f)
                        self.__dict__[attr] = t
                        f.close()
                except:
                        print 'Reading marshal failed.\r\n'

        def __updateDbInRom(self, attr):
                try:
                        f = open('_'+attr.upper(),'wb') # opening it for writting will erase the file.
                        marshal.dump(self.__dict__[attr], f)
                        f.close()
                except:
                        print 'Writing marshal failed.\r\n'
                        raise
        
                
        def retrieve(self, permitLevel):
                return self.__str__()

        def __str__(self, noAlias = 0): #default is to print alias
                formatter = '%-17s\t%s\r\n'
                #scramblePwd = lambda pwd: '*'*len(pwd)
                #rep = formatter % ('Pwd:', scramblePwd(str(self.adminaccount['pwd'])))
                printer = []
                if not self.adminaccount.has_key('admin'):
                        printer.append('<NO USERS>\r\n')
                else:
                        if noAlias:
                                printer.append(formatter % ('Admin:', self.adminaccount['admin']))
                        else:
                                printer.append(formatter % ('Admin:', self.replaceByOriginalPhone(self.adminaccount['admin'])))
                
                #print gprssettings
                printer.append('\r\nGPRS: \r\n')
                if len(self.gprssetting) == 0:
                        printer.append('<>\r\n')
                else: 
                        fields = ('APN', 'USERID', 'PASSW', 'IP', 'PORT_NO')
                        for index in range(5):
                                printer.append(formatter % (fields[index], self.gprssetting[index]))
                
                printer.append('\r\n')
                printer.append(formatter % ('Device:','Listener:'))
                
                if len(self.devicelist) == 0:
                        printer.append('<>\r\n')
                else: 
                        for ieeeHex in self.devicelist.keys():
                                if noAlias:
                                        printer.append(formatter % (self.Hex2Str(ieeeHex,cachethis=1),str(self.devicelist[item])))

                                else:
                                        aliasIeee = self.replaceByIEEEAlias(ieeeHex)
                                        aliasNos = map(self.replaceByPhoneAlias,self.devicelist[ieeeHex])
                                        printer.append(formatter % (aliasIeee+':', ','.join(aliasNos)))
                                        #rep = rep + aliasIeee + ':\t'+ ','.join(aliasNos) +'\r\n'
                
                printer.append('\r\n')
                printer.append(formatter % ('Reminder:','AlertTime:'))
                
                if len(self.reminderlist) == 0:
                        printer.append('<>\r\n')
                else: 
                        for item in self.reminderlist.keys():
                                if noAlias:
                                        printer.append(formatter % (item+':', ':'.join(self.reminderlist[item])))
                                else:
                                        aliasIeee = self.replaceByIEEEAlias(item)
                                        rmdr = self.reminderlist[item]
                                        printer.append(formatter % (aliasIeee+':', str(rmdr/3600)+':'+str(rmdr%3600/60)))
                
                #print aliasList
                if not noAlias:
                        printer.append('\r\nAliases:\r\n')
                        allphones = self.phonealiaslist.items()
                        #below doesn't work in python 1.x
                        #acopyitems.sort(cmp = lambda firstTuple, secondTuple: len(firstTuple[0])-len(secondTuple[0]))
                        allphones.sort()

                        allieees = self.ieeealiaslist.items()
                        allieees.sort()
                        
                        if not len(allphones) and not len(allieees):
                                printer.append('<>\r\n')
                        else: 
                                for origin, alias in allphones:
                                        printer.append(formatter % (origin+':', alias))
                                        
                                for origin, alias in allieees:
                                        printer.append(formatter % (self.Hex2Str(origin,cachethis=1)+':', alias))
                
                printer.append('\r\n')
                printer.append(formatter % ('Routers:','Alias:'))

                allrouters = self.routeraliaslist.items()
                
                if not allrouters:
                        printer.append('<>\r\n')
                else: 
                        allrouters.sort()
                        for router, alias in allrouters:
                                printer.append(formatter % (self.Hex2Str(router,cachethis=1)+':', alias))
                                
                printer.append('\r\n')
                return ''.join(printer)
                
        def checkPermitLevel(self, phone_number):
                if phone_number == 'gprs' or phone_number == 'serial':
                        return ADMIN_LEVEL
                
                if len(phone_number)<8:
                        #print 'This is not a valid phone_number.\r\n'
                        return NON_USER_LEVEL
                
                if len(self.adminaccount) == len(FACTORY_ADMINACCOUNT) or self.adminaccount['admin'].endswith(phone_number[-8:]):
                        return ADMIN_LEVEL
                        
                all_phone_numbers = self.getAllPhones()
                for num in all_phone_numbers:
                        if num.endswith(phone_number[-8:]):
                                return USER_LEVEL
                
                return NON_USER_LEVEL

                
        def __removeDuplicates(self, mylist):
                if mylist:
                        mylist.sort()
                        last = mylist[-1]
                        for i in range(len(mylist)-2, -1, -1):
                                if last == mylist[i]:
                                        del mylist[i]
                                else:
                                        last = mylist[i]
                return mylist
            
        def __flattenListofLists(self, mylist):
                flattened=[]
                for nums in mylist:
                        flattened.extend(nums)              
                return flattened
        
        def Hex2Str(self, ieeeHex, cachethis = 0, cache={}): #don't cache all ieeeHex as it is called by shell also.
                if cache.has_key(ieeeHex):
                        return cache[ieeeHex]
                else:
                        result = ''.join(map(lambda h: h[-2:] ,map(hex, map(ord,ieeeHex)))).replace('x','0') #fastest method so far
                        if cachethis:
                                cache[ieeeHex] = result
                        return result
                        
        def Str2Hex(self, ieeeStr, cachethis = 0, cache={}):
                if cache.has_key(ieeeStr):
                        return cache[ieeeStr]
                else:
                        ieeeHex = ''
                        if len(ieeeStr) % 2 == 1:
                            ieeeStr = '0'+ieeeStr
                        ieeeHexLong = long(ieeeStr,16)
                        for shift in range ((len(ieeeStr)/2-1)<<3, -8, -8):
                            ieeeHex = ieeeHex + chr((ieeeHexLong>>shift)&0xff)

                        if cachethis:
                                cache[ieeeStr] = ieeeHex
                        return ieeeHex

# if __name__ == "__main__":
        # db = Database()
        
        # print '\n'*2+'/'*80 + '\n' +'/'*80
        # #print db.changePassword('12345',ADMIN_LEVEL)
        # x = {'0123456789abcdef':'6598765432','00000000ffffffff':'6598760000', '0000000011111111':'98760000'}
        # db.format2Factory('98765432')
        
        # db.changeAdmin('12345678',ADMIN_LEVEL)
        # db.gprssetting=('"hicard"','""','""','"www.malresearch.nus.edu.sg"','"5309"')
        # print db.store('98989898',['0123456789abcdef','00000000fffffffx'], ADMIN_LEVEL)
        # print db.store('6598989898',['0123456789abffff'], ADMIN_LEVEL)
        # print db.store('6598989898',['0123456789abffff'], ADMIN_LEVEL)
        # print db.store('+6598989898',['0123456789abfff0'], ADMIN_LEVEL)
        # print db.store('006598760000',['00000000ffffffff'], ADMIN_LEVEL)
        # print db.store('98760000',['00000000ffff0000'], ADMIN_LEVEL)
        # print db.store('+6598760000',['0123456789abffff'], ADMIN_LEVEL)
        # print db.store('006598760000',['0123456789abcdef'], ADMIN_LEVEL)
        # print db.store('006511111111',['0123456789abcdee'], ADMIN_LEVEL)
        # print db.store('006511111111',['0123456789abffff'], ADMIN_LEVEL)
        # print db.store('yuan_jian',['0123456789000000'], ADMIN_LEVEL)
        # print db

        # print 'START UPDATING ALIAS\r\n'
        
        # print db.updateAlias([['98989898','yuan_jian11']], ADMIN_LEVEL)
        # print db.updateAlias([['98760000','jian_nan']], ADMIN_LEVEL)
        # print db.updateAlias([['00ffffffff','old333']], ADMIN_LEVEL)
        # print db.updateAlias([['0123456789abfff0','old333']], ADMIN_LEVEL)
        # print db.updateAlias([['00ffffffff','old333']], ADMIN_LEVEL)
        
        # print db.updateAlias([['old333','old22'],['old22','old1']], ADMIN_LEVEL)
        # print db.updateAlias([['6789abcdef','old2']], ADMIN_LEVEL)
        # print db.updateAlias([['6789abcdef',''],['6789abcdef','old2'],['yuanjian','yuan_jian'],['yuan_jian11','yuan_jian']], ADMIN_LEVEL)
        
        # print db.updateAlias([['6789abffff','old3']], ADMIN_LEVEL)
        # print db.updateAlias([['0123456789abfff0','old4'],['00000000ffff0000', 'old5']], ADMIN_LEVEL)

        # print 'STORING AGAIN\r\n'
        # print db.store('yuan_jian',['0123456789000000'], ADMIN_LEVEL)
        # print db.store('yuan_jian',['0123456789000000'], ADMIN_LEVEL)
        # print db.store('yuan_jian',['old11'], ADMIN_LEVEL)
        # print db.store('yuan_jian22',['old1'], ADMIN_LEVEL)
        
        # print db
        
        # print 'DELETING\r\n'
        # print db.delete(['old2', '11111111'], ADMIN_LEVEL)
        # print db.delete(['jian_nan', 'old2', '11111111'], ADMIN_LEVEL)
        # print db.delete(['jian_nan', 'old2', '11111111'], ADMIN_LEVEL)
        # print db.delete(['old2'], ADMIN_LEVEL)
        # print db.delete(['6789abcdef','old3'], ADMIN_LEVEL)
        # print db.delete(['0123456789abcdee','old6'], ADMIN_LEVEL)
        # print db

        # print db.reminder(12*3600+30*60,['98989898','yuan_jian11'], 'admin', ADMIN_LEVEL)
        # print db.reminder(12*3600+30*60,['98989898','yuan_jian'], 'admin', ADMIN_LEVEL)
        # print db.reminder(12*3600+30*60,['all','yuan_jian'], 'admin', ADMIN_LEVEL)
        # print db.reminder(12*3600+30*60,['all'], '98989898', USER_LEVEL)

        # print db.updateAlias([['0123456789abaff0','old44'],['98989894', 'jian55']], ADMIN_LEVEL)
        # print db

        # print '\n'*2+'/'*80 + '\n' +'/'*80
        # #print db.changePassword('12345',ADMIN_LEVEL)
        # x = {'0123456789abcdef':'6598765432','00000000ffffffff':'6598760000', '0000000011111111':'98760000'}
        # db.format2Factory('98765432')
        
        # db = Database()
        # print db
        # db.format2Factory('98765432')
        # db.changeAdmin('12345678',ADMIN_LEVEL)
        # print db.store('98989898',['0123456789abcdef','0123456789abcdff', '0123456789abcd00'], ADMIN_LEVEL)
        # print db.updateAlias([['98989898','jian'],['0123456789abcdef','old1'],['0123456789abcdff','old2'],['0123456789abcd00','old3']], ADMIN_LEVEL)
        # print db.store('98765432', ['old1','old2','old3'],ADMIN_LEVEL)
        # print db.updateAlias([['98765432','nan']], ADMIN_LEVEL)
        # #print db.delete(['jian'], ADMIN_LEVEL)
        # print db.delete(['6598989898'], ADMIN_LEVEL)
        # print db
