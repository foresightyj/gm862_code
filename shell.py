import MDM	#accessing AT command interface
import MOD	#for timers
import SER
import sys

from sms import Sms
from gprs import Socket

ADMIN_LEVEL = 2
USER_LEVEL = 1
NON_USER_LEVEL = 0

ALL_SUCCESS = 0
IEEE_SUCCESS = 1
PHONE_SUCCESS = 2
UNAUTHROIZED = 3
INVALID = 4

class Shell:
	#Initialize GSM and SMS
	def __init__(self, system):
		#SER.send('Initialize Command Processor!\r\n')
		self.sms = Sms() #create a new Sms Object as only Command will use Sms
		self.system = system # pass in self.system by reference, so in system, Command only has access to the database
		#SER.send('GPRS Setting: '+ str(self.system.database.getGprsSetting()))
		if (self.system.database.getGprsSetting()):
			self.socket = Socket(self.system)
		else:
			self.socket = 0 # Will be set when GPRS == configured later via SMS.
		
		#SER.send('Command Processor Initialized.\r\n')
		self.serialMsgList = ''
		self.SERIALENABLED = not self.system.cc2430.CC2430ENABLED

	
	def processShell(self):
		self.checkMsg()
		orders = self.dissectMsg()
		query_resp = 0	# mark if needs to send GPRS and sensor data via SMS in response to a query.
		for message in orders:
			msgIndex, inquirer, content = message
			
			permit = self.system.database.checkPermitLevel(inquirer)
			
			content_list = content.split()
			
			if not content_list: #content_list might be empty
				continue
			
			if self.SERIALENABLED and sys.modules.has_key('SER'):
				SER.send('\r\n'+ '-'*50+'\r\n')
				SER.send('From: '+ inquirer+'\r\n')
				SER.send('Content: '+content+'\r\n')
				SER.send('Permit: '+str(permit)+'\r\n\r\n')
			
			# it turns out the crappy sms dissection returns the following when balance message is received.
			# ['1', '1344","Voicemail","12/07/11,19:04:07+32"', 'your hi!card balance is $ 8.04 and your expiry date is 09 oct 2012.']
			# tentatively, just check the length of the inquirer and if contents contain the string 'balance'
			if len(inquirer) > 15 and content.find('balance') > -1:
				#just forward this message to the admin
				if self.system.LISTENER_EN:
					self.sendMsg(self.system.database.getAdmin(),'e-Guardian:'+content)
				elif self.socket:
					self.socket.sendGprsMsg('e-Guardian:'+content)
				
				#delete the message
				self.deleteMsg(msgIndex)
				break
				
			replyto = ''
			reply_msg =''
			
			if content_list[-1] == 'togprs':
				content_list.pop()
				replyto = 'gprs'
			elif content_list[-1] == 'toserial':
				content_list.pop()
				if self.SERIALENABLED and sys.modules.has_key('SER'):
					replyto ='serial'
				else:
					replyto = inquirer
					reply_msg = 'Serial Not Supported.\r\n' + reply_msg
			elif content_list[-1] == 'tosms':
				content_list.pop()
				#send to administrator
				replyto = self.system.database.getAdmin()
			else: #no specified
				replyto = inquirer
			
			try:
				command, command_args = content_list[0], content_list[1:]
				command = command.lower()
				command = filter(lambda l: l>='a' and l<='z' or l in ('?','!'), command) 
							# only allow alphabetical characters with exceptions of ? and ! in command

				if permit and (command == 'querystatus' or command == 'querystatus!'):
					# If a specific IEEE == inquired, just send to that device.
					# Otherwise:
					#      If admin inquires, send to all wearable devices at once, but apply some delay (probably 1 second)
					#      after each wearable device
					
					#      If user inquires, send to all IEEE addrs associated to that user.
					if permit in (USER_LEVEL, ADMIN_LEVEL):
							ieeeOrPhoneNoList = command_args
							if ieeeOrPhoneNoList: #not empty
									this_reply = ''
									if command == 'querystatus!':
											# querystatus! will seek those offline devices before querying
											action = lambda ieeeHex, \
													add2MsgPool=self.system.add2MsgPool, \
													msgid=self.system.cc2430.STATUS_REQ_MSG_ID:\
													add2MsgPool(msgid, ieeeHex)
									else: #querystatus will not send to offline devices.
											action = lambda ieeeHex, \
													add2MsgPool=self.system.add2MsgPool, \
													msgid=self.system.cc2430.STATUS_REQ_MSG_ID, \
													isOnline=self.system.isOnline: \
													not isOnline(ieeeHex) or add2MsgPool(msgid, ieeeHex) #return 1 if not online
									
									status,failed = self.system.database.actOnTargets(ieeeOrPhoneNoList, action, inquirer, permit)
									
									for i in range(len(ieeeOrPhoneNoList)):
											if status[i] == ALL_SUCCESS:
													if permit == ADMIN_LEVEL:
															this_reply = this_reply + 'Query sent to all devices.\r\n'
													else:
															this_reply = this_reply + 'Query sent to all devices associated with %s.\r\n' % self.system.database.replaceByPhoneAlias(inquirer[-8:])
											elif status[i] == IEEE_SUCCESS:
													this_reply = this_reply + 'Query sent to %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == PHONE_SUCCESS:
													this_reply = this_reply + 'Query sent to all devices associated with %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == UNAUTHROIZED:
													this_reply = this_reply + 'You are unauothorized to access %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == INVALID:
													this_reply = this_reply + 'Unknown device or phone no: %s.\r\n' % ieeeOrPhoneNoList[i]
									if failed:
											this_reply = this_reply + ','.join(map(self.system.database.replaceByIEEEAlias, failed)) + ' found offline.\r\n'
							else:
									this_reply = 'No argument for querystatus.\r\n'
							
							if replyto == 'gprs':
									reply_msg = reply_msg + this_reply
				
				elif permit and command == 'active':
						status = self.system.getActiveStatus()
						active = filter(lambda st: st[2], status) #st[1] is 1 for active devices and 0 otherwise
						inactive = filter(lambda st: not st[2], status)
						reply_msg = ''
						if active:
							reply_msg = reply_msg + 'Active WD:\t%s\r\n' % ','.join(map(lambda st, Hex2Str = self.system.database.Hex2Str: '%s(%s)' % (st[0], Hex2Str(st[1])), active))
						if inactive:
							reply_msg = reply_msg + 'Inactive WD:\t%s\r\n' % ','.join(map(lambda st, Hex2Str = self.system.database.Hex2Str: '%s(%s)' % (st[0], Hex2Str(st[1])), inactive))
						
						nonDead = map(lambda st: st[0], status)
						dead = []
						for ieeeHex in self.system.database.devicelist.keys():
							ieeeAlias = self.system.database.replaceByIEEEAlias(ieeeHex)
							if ieeeAlias not in nonDead:
								dead.append(ieeeAlias)
								
						if dead:
							reply_msg = reply_msg + 'Dead WD:\t%s\r\n' % ','.join(dead)
						
						routerStatus = self.system.getRouterStatus()
						activeRouters = filter(lambda st: st[2], routerStatus)
						inactiveRouters = filter(lambda st: not st[2], routerStatus)
						
						if activeRouters:
							reply_msg = reply_msg + 'Active RE:\t%s\r\n' % ','.join(map(lambda st, Hex2Str = self.system.database.Hex2Str: '%s(%s)' % (st[0], Hex2Str(st[1])), activeRouters))
						if inactiveRouters:
							reply_msg = reply_msg + 'Inactive RE:\t%s\r\n' % ','.join(map(lambda st, Hex2Str = self.system.database.Hex2Str: '%s' % st[0], inactiveRouters))

						#Active and Inactive routers retrieves from __ROUTER_TABLE which at least appeared in the system once.
						#There might be routers that are added using 'router' command but never showed up in the system, call them dead routers
						nonDeadRouter = map(lambda st: st[0], routerStatus)
						deadrouters = []
						for router in self.system.database.routeraliaslist.values():
							if router not in nonDeadRouter:
								deadrouters.append(router)
						
						if deadrouters:
							reply_msg = reply_msg + 'Dead RE:\t%s\r\n' % ','.join(deadrouters)

				elif command == 'format': # allow non-user to format it so permit is not checked
					if self.system.database.checkPassword(command_args[0]): #no need to check permit as long as the pwd == correct
						gprsTuple = self.system.database.getGprsSetting()
						
						if inquirer == 'gprs' or inquirer == 'serial':
							admin_no = command_args[1]
							reply_msg = reply_msg + self.system.database.format2Factory() #reset database
							
							#also make this no the admin
							permit = ADMIN_LEVEL
							reply_msg = reply_msg + self.system.database.changeAdmin(admin_no, permit)
						else:
							reply_msg = reply_msg + self.system.database.format2Factory() #reset database
							
							#also make this no the admin
							permit = ADMIN_LEVEL
							reply_msg = reply_msg + self.system.database.changeAdmin(inquirer, permit) #set this no as admin
						
						#also need to close GPRS connection
						if self.socket and inquirer != 'gprs':
							self.socket.closeSocket()
							MOD.sleep(20) # apply some short pause
						else:
							#still keep the same gprs setting in flash.
							if gprsTuple:
								self.system.database.updateGprsSetting(gprsTuple, permit) # With ADMIN_LEVEL
					else: #Wrong password
						reply_msg = reply_msg + 'Wrong password. Pls retry.\r\n'
					
				elif permit and command == 'changepwd':
					reply_msg = reply_msg + self.system.database.changePassword(command_args[0],permit)
					
				elif permit and command == 'store':
					reply_msg = reply_msg + self.system.database.store(command_args[0], command_args[1:], permit)
				
				elif permit and command == 'delete':
					reply_msg = reply_msg + self.system.database.delete(command_args, permit)
				
				elif permit and command == 'retrieve':
					reply_msg = reply_msg + self.system.database.retrieve(permit)
					
				elif permit and command == 'allowjoin':
							ieeeOrPhoneNoList = command_args
							if ieeeOrPhoneNoList: #not empty
									action = lambda ieeeHex, add2MsgPool=self.system.add2MsgPool, msgid=self.system.cc2430.CONTROLLED_IEEE_REQ_MSG_ID: add2MsgPool(msgid, '', ieeeHex)
									
									mydatabase = self.system.database
									status = mydatabase.actOnTargets(ieeeOrPhoneNoList, action, inquirer, permitLevel)[0]
									this_reply = ''
									for i in range(len(ieeeOrPhoneNoList)):
											if status[i] == mydatabase.ALL_SUCCESS:
													if permitLevel == ADMIN_LEVEL:
															this_reply = this_reply + 'Allowjoin sent to all devices.\r\n'
													else:
															this_reply = this_reply + 'Allowjoin sent to all devices associated with %s.\r\n' % mydatabase.replaceByPhoneAlias(inquirer[-8:])
											elif status[i] == mydatabase.IEEE_SUCCESS:
													this_reply = this_reply + 'Allowjoin sent to %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == mydatabase.PHONE_SUCCESS:
													this_reply = this_reply + 'Allowjoin sent to all devices associated with %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == mydatabase.UNAUTHROIZED:
													this_reply = this_reply + 'You are unauothorized to access %s.\r\n' % ieeeOrPhoneNoList[i]
											elif status[i] == mydatabase.INVALID:
													this_reply = this_reply + 'Unknown device or phone no: %s' % ieeeOrPhoneNoList[i]
							else:
									this_reply = 'No argument for allowjoin.\r\n'
									
							if replyto == 'gprs':
									reply_msg = reply_msg + this_reply
							
				elif permit and command == 'reminder':
					# register the reminder in the system, only alert to corresponding wearable device when timed up.
					try:
						if command_args[0] == 'delete':
							hhmmss = 0
						else:
							if len(command_args[0])>4:
								raise
							
							mm = int(command_args[0][-2:])
							hh = int(command_args[0][-4:-2])
							
							if hh >= 0 and hh < 24 and mm >= 00 and mm < 60:
								hhmmss = hh*3600+mm*60
							else:
								raise
						
						reply_msg = reply_msg + self.system.database.reminder(hhmmss, command_args[1:], inquirer, permit)
						
					except:
							#invalid time format
							reply_msg = reply_msg + 'Wrong Time Format.\r\n'
							raise
							
				elif permit and (command == 'alias' or command == 'router'):
					#create alias for phone numbers, device ids
					# alias 96368893='Yuan Jian' 2726252423220009=Sam 2423220001=Paul
					# with no space between them, if any alias has spaces in them, use "'" or '"' to quote them

					parseAlias = lambda alias: filter(lambda part: len(part.strip())>0, alias.split('='))

					allValidAliases = filter(lambda alias: len(alias)==2, map(parseAlias, command_args))
					
					if len(allValidAliases) == len(command_args): #meaning all aliases are valid
						if command == 'alias':
							reply_msg = reply_msg + self.system.database.updateAlias(allValidAliases, permit)
						else: # router
							reply_msg = reply_msg + self.system.database.addRouters(allValidAliases, permit)
					else:
						#not all arguments are correct
						reply_msg = reply_msg + 'Wrong Alias Format.\r\n'
				
				elif permit and command == 'backdoor':
					#for backdoor code, will be removed in the release
					reply = ''
					#since commands are split according to whitespaces
					#join them back
					self.socket
					lines = ' '.join(command_args)
					#lines could be something like "'x = 1 + 2' 'x = x + 1' 'print x'"
					#split them again
					lines = lines.replace("' '", "'\n'")

					lines = lines.split('\n')
					
					isQuote = lambda c: c == '"' or c == "'"
					for line in lines:
						if line and isQuote(line[0]) and isQuote(line[-1]):
							isstatement= 0
							try:
								code= compile(line[1:-1], '<string>', 'eval')
							except SyntaxError:
								isstatement= 1
								code= compile(line[1:-1], '<string>', 'exec')
							
							try:
								if isstatement:
									exec code
								else: #expression
									reply = reply + str(eval(code))+'\r\n'
							except:
								exc_type, _, _ = sys.exc_info()
								reply = reply + 'Backdoor Exception: '+exc_type+'\r\n'
						else:
							reply = reply + 'Code not quoted. Abort!\r\n'
							break
					reply_msg = reply_msg + reply
					
				elif permit and command == 'initgprs':
					if permit == ADMIN_LEVEL:
						#init_gprs(APN, GPRS_USERID, GPRS_PASSW, IP, PORT_NO)
						
						wrongquotes = 0
						checkQuotes =  lambda str1 : str1.startswith('"') and str1.endswith('"')
						for j in range(5):
							if not checkQuotes(command_args[j]):
								wrongquotes = 1
						if wrongquotes == 0:
							gprsTuple = tuple(command_args)
							#set GPRS settings
							if self.socket:
								#if a socket exists right now, close the socket first
								self.socket.closeSocket()
								MOD.sleep(20) # apply some short pause
							
							self.system.database.updateGprsSetting(gprsTuple, permit) # With ADMIN_LEVEL
							self.socket = Socket(self.system)
							
							reply_msg = reply_msg + 'GPRS configured.\r\n'
					
				elif permit and command == 'socketsend':
					if permit == ADMIN_LEVEL and self.socket:
						self.socket.sendGprsMsg(' '.join(command_args)+'\r\n')
				
				elif permit and command == 'quit':
					if permit == ADMIN_LEVEL:
						#if self.socket:
						#	self.socket.closeSocket()
						if self.socket:
							self.socket.sendGprsMsg('internalquit\r\n')
							MOD.sleep(30)
							self.socket.closeSocket()
						
						MOD.sleep(20)	
						sys.exit() #exit out of program.
					
				elif permit and command == 'shutdown':
					if permit == ADMIN_LEVEL:
						#SER.send('Rebooting System ...\r\n')
						#start rebooting
						
						self.deleteMsg(msgIndex) #must delete the message
						
						if self.socket:
							self.socket.sendGprsMsg('internalquit\r\n')
							MOD.sleep(30)
							self.socket.closeSocket()
						
						MOD.sleep(20)
						MDM.send('AT#SHDN\r',2)
						res = MDM.receive(100)
						while(res.find('OK')<0):
							MDM.send('AT#SHDN\r',0)
							#SER.send('Keep shutting down until success.\r\n')
							res = MDM.receive(50)
					
				elif permit and command == 'reboot':
					if permit == ADMIN_LEVEL:
						#SER.send('Rebooting System ...\r\n')
						#start rebooting
						
						self.deleteMsg(msgIndex) #must delete the message
						
						if self.socket:
							self.socket.sendGprsMsg('internalquit\r\n')
							MOD.sleep(30)
							self.socket.closeSocket()							
						
						MOD.sleep(20)
						MDM.send('AT#REBOOT\r',2)
						res = MDM.receive(100)
						while(res.find('OK')<0):
							MDM.send('AT#REBOOT\r',0)
							#SER.send('Keep rebooting until success.\r\n')
							res = MDM.receive(50)
				
				elif permit and command == 'balance':
					if permit == ADMIN_LEVEL:
						#check balance
						reply_msg = self.system.checkBalance()
						
				elif permit and command == 'sms':
					if permit == ADMIN_LEVEL:
						reply_msg = reply_msg + self.system.database.updateSmsEnable(command_args[0], lambda enable, setSmsEnable=self.system.setSmsEnable: setSmsEnable(enable))
						
				elif permit and command == 'trigger':
					if permit == ADMIN_LEVEL:
						reply_msg = reply_msg + self.system.database.updateTrigger(int(command_args[0]), lambda trigger, setDiffTrigger=self.system.setDiffTrigger: setDiffTrigger(trigger))
								
				elif permit and command == 'debug':
					if permit == ADMIN_LEVEL:
						if command_args[0] == 'on':
							self.system.DEBUG_EN = 1
							reply_msg = 'Debug on.\r\n'
						elif command_args[0] == 'off':
							self.system.DEBUG_EN = 0
							reply_msg = 'Debug off.\r\n'
				elif permit:
					reply_msg = reply_msg + 'Command "'+ command +'" is not supported.\r\n'
				
			except IndexError:
				reply_msg = 'Not enough command parameters. Pls retry.\r\n'
			except SystemExit:
				raise # re-raise the exception
			except:
				
				reply_msg = self.system.printStackTrace('Unknown error. Pls retry.\r\n')
				if self.socket:
					self.socket.sendGprsMsg(reply_msg)
					self.socket.closeSocket()
				
				print reply_msg
				
			#delete the message
			self.deleteMsg(msgIndex)

			if permit in (USER_LEVEL, ADMIN_LEVEL): #only attend to valid users and do not respond to a query message.
				#SER.send('Reply msg is sent to : '+replyto+'\r\n')
				if reply_msg != '':
					self.sendMsg(replyto,reply_msg)
				if sys.modules.has_key('SER') and self.SERIALENABLED and replyto == 'serial':
					print '-'*30
					print self.system.database
	
	def sendMsg(self, to, msg):
		if (to == 'gprs'):  #GPRS mode
			if self.socket:
				self.socket.sendGprsMsg(msg) #Only send to 1 port.
		elif(to == 'serial') and self.SERIALENABLED:
			SER.send(msg)
			pass
		else:#SMS mode
			self.sms.sendSms(to, msg)
		
	def checkMsg(self): #Check Both SMS and GPRS.
		self.sms.checkNewSms()
		if self.socket:
			self.socket.checkNewGprsMsg()
		
	def deleteMsg(self, msg_no):
		if (msg_no == 'gprs'): #GPRS mode
			pass
		elif (msg_no == 'serial'): #Serial mode
			pass
		else: #SMS mode
			self.sms.deleteSms(msg_no)

	def getLine(CONN): # CONN must support .receive(timeout) method.
		res = ''
		r = 'r'
		while not res.endswith('\r') and r != '':
			r = CONN.receive(2)
			res = res + r
			
		return res
	
	def dissectMsg(self):
		# msg no 1 + sender no 1 + commands   ---------- 1st message
		# msg no 2 + sender no 2 + commands   ---------- 2nd message
		#......................................................
		
		# If msg_no and sender_no are both 'gprs', that means the message == a gprs msg.
		# If msg_no and sender_no are both 'serial', that means the message == a serial port msg.
		
		# 1. Dissect SMS
		new_list=''
		new_list=self.sms.smsList.split('\n')
		
		#construct a table to hold the 2 dimensional string array
		i=0
		table=[]

		for string in new_list:
			if string.find('+CMGL:') != -1:
				first_occur = string.find('+CMGL:')+7
				last_occur = string.find(',')
				msg_no=string[first_occur:last_occur]
				first_occur = string.find('","')+3
				last_occur = string.find('","","')
				sender_no = string[first_occur: last_occur]

				#jump to next row which contains the actual SMS command
				content = new_list[i+1]	#SMS content
				x=content.split('\r')
				content=x[0].lower()
				temp = [msg_no, sender_no, content]
				if len(sender_no) == 6 and content.find('reply via sms with option no')>-1:
					self.sendMsg(sender_no, '2')
				table.append(temp)
			i=i+1
		
		# 2. Dissect GPRS MSG
		if self.socket:
			new_list=self.socket.gprsMsgList.split('\r')
			#Only process the part that == before the last \n
			if new_list[-1] == '':
				#That means it == a complete msg, then clear gprsMsgList
				self.socket.gprsMsgList = ''
			else:
				# pop out (remove) the last entry, assign it to gprsMsgList
				self.socket.gprsMsgList = new_list.pop() 
				
			for string in new_list:
				string = self.__process_backspaces(string.strip())
				msg_no = 'gprs'
				sender_no = msg_no
				temp = [msg_no, sender_no, string]
				table.append(temp)
				
				if string.find('syncTimeRsp') > -1:
					index = string.find('syncTimeRsp')
					timestr = string[index+len('syncTimeRsp'): index+len('syncTimeRsp')+17] #eg. "11/11/30,12:39:25", length = 17
					self.system.time.syncTimeRsp(timestr)
					
					
		# 3. Dissect Serial Port MSG
		if self.SERIALENABLED and sys.modules.has_key('SER'):
			#SER.send("\r\nRetrieving msg from serial port.\r\n")
			res = 'r'
			#retrieve commands
			while res != '':
				res = SER.receive(2)
				self.serialMsgList = self.serialMsgList + res
			
			
			#process commands
			if self.serialMsgList != '':
				new_list = self.serialMsgList.split('\r')
				
				if new_list[-1] == '':
					#That means it == a complete msg, then clear gprsMsgList
					self.serialMsgList = ''
				else:
					# pop out (remove) the last entry, assign it to gprsMsgList
					self.serialMsgList = new_list.pop()
				
				for string in new_list:
					string = self.__process_backspaces(string.strip())
				
					msg_no = 'serial'
					sender_no = msg_no
					temp = [msg_no, sender_no, string]
					table.append(temp)
	
		return table	#separating the contents into single words
		
	def __process_backspaces(self, input, token='\b'):
		output = ''
		for item in (input+' ').split(token):
			output = output + item
			output = output[:-1]
		return output
