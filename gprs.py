import MDM2
import MDM
import MOD
import SER
import sys

class GPRSWriter:
    def write(self,s):
		SER.send(s+'\r')
		MDM2.send(s.replace('\r', '\n'),0)
		
class Socket:
	def __init__(self, system):
		APN, USERID, PASSW, IP, PORT_NO = system.database.getGprsSetting()
		self.WELCOME_MSG = '\r\nGM862 ' + system.getImei()+ ' Ready\r\n'
		#APN='"hicard"', GPRS_USERID='""', GPRS_PASSW='""', IP='"137.132.165.174"', PORT_NO='"5301"'
		self.gprsMsgList = ''
		self.noCarrierCount = 0
		self.backCount = 400
		self.IP = IP.replace('"', '') #get rid of surounding '"'
		self.PORT_NO = PORT_NO.replace('"', '')
		
		self.SERIALENABLED = not system.cc2430.CC2430ENABLED

		res = MDM2.send('AT+CGDCONT=1,"IP",'+APN.replace('"', '')+'\r',0)
		res = MDM2.receive(10)
		##SER.send(res)

		#res = MDM2.send('AT#SCFG=1,1,512,3600,600,50\r',0)
		res = MDM2.send('AT#SCFG=1,1,512,3600,600,10\r',0)
		# associate the Connection Id number 1 to the context number 1 with a 
		# minimum packet size of 512 bytes, global timeout of 3600 sec, 
		# connection timeout of 60 sec and transmission timeout of 5 sec.
		res = MDM2.receive(10)
		##SER.send(res)
		
		##SER.send('AT#SGACT=1,0\r\n')
		res = MDM2.send('AT#SGACT=1,0\r',0)
		res = MDM2.receive(20)
		##SER.send(res)

		##SER.send('AT#SGACT=1,1\r\n')
		res = MDM2.send('AT#SGACT=1,1\r',0)
		res = MDM2.receive(40)
		##SER.send(res)
		
		retry = 0
		while res.find('OK') < 0 and retry < 100:
			retry = retry + 1
			##SER.send('Retry until OK\r\n')
			res = MDM2.send('AT#SGACT=1,1\r',0)
			res = MDM2.receive(40)
			##SER.send(res)
		
		##SER.send('AT#USERID\r\n')
		res = MDM2.send('AT#USERID='+USERID+'\r',0)
		res = MDM2.receive(10)
		##SER.send(res)
		
		##SER.send('AT#PASSW\r\n')
		res = MDM2.send('AT#PASSW='+PASSW+'\r',0)
		res = MDM2.receive(10)
		##SER.send(res)

		#No need to close the socket as it == automatically closed after timeout.
	
		#SER.send('AT#PADFWD='+str(ord('\n'))+'\r\n')
		#res = MDM2.send('AT#PADFWD='+str(ord('\n'))+'\r',0) # sending '\r' will result in immediate flush to socket. 
		#res = MDM2.receive(20)
		#SER.send(res)

		#SER.send('AT#PADCMD=1\r\n')
		#res = MDM2.send('AT#PADCMD=1\r',0)
		#res = MDM2.receive(20)
		#SER.send(res)
	
		res = self.openSocket()
		##SER.send(res)
		
		MDM2.send(self.WELCOME_MSG,0)
		
	def checkNewGprsMsg(self): # check all new messages
		res='r'
		while res != '':
			res = MDM2.receive(1) #originally 5
			self.gprsMsgList = self.gprsMsgList + res
		
		if len(res.strip())>1: #received something from socket so reset round.
			self.backCount = 400
		
		self.backCount = self.backCount - 1

		if not self.backCount:
			#echo some message to twisted server and wait for message back.
			#if self.SERIALENABLED:
			#	SER.send("ECHO!!\r\n")
			
			MDM2.send(chr(0xee)+'\r\n',0) #\xee
			#and wait for the same Bell character back as acknowledgement from twisted server.
			cnt = 0
			res = ''
			while cnt < 10:
				cnt = cnt + 1
				res = res + MDM2.receive(5)
				if res.find(chr(0xee)) > -1: #found
					#if self.SERIALENABLED:
					#	SER.send("Break at%d!!\r\n" % cnt)
					break
			
			self.gprsMsgList = self.gprsMsgList + res.replace(chr(0xee),'')
			if res.find(chr(0xee))>-1:
				#found the echo
				self.noCarrierCount = 0
				self.backCount = 400
			else:
				self.backCount = self.backCount + 1 # we want if clause to be enterred again in the next round
				self.noCarrierCount = self.noCarrierCount + 1
		
		if self.noCarrierCount > 0 or self.gprsMsgList.find('NO CARRIER') > -1 or self.gprsMsgList.find('ERROR') > -1:
			#if self.SERIALENABLED:
			#	SER.send("self.gprsMsgList = %s" % self.gprsMsgList)
			self.gprsMsgList = self.gprsMsgList.replace('NO CARRIER', '').replace('ERROR', '')
			self.noCarrierCount = self.noCarrierCount + 1
			if self.noCarrierCount > 3:
				if self.SERIALENABLED:
					SER.send("REBOOT\r\n")
				MOD.sleep(20)
				MDM.send('AT#REBOOT\r',2)
				res = MDM.receive(100)
				while(res.find('OK')<0):
					MDM.send('AT#REBOOT\r',2)
					if self.SERIALENABLED:
						SER.send('Keep rebooting until success.\r\n')
					res = MDM.receive(50)
			
			#close socket first
			self.closeSocket()
			#Reopen the socket.
			res = self.openSocket()
			
			if res.find('CONNECT') > -1:
				self.noCarrierCount = 0
				self.sendGprsMsg(self.WELCOME_MSG)
			
			if self.SERIALENABLED:
				SER.send(res)
			
	def sendGprsMsg(self, msg):
		#send this message.
		##SER.send(msg)
		res = MDM2.send(msg,0) #no concatenation of strings.
		res = MDM2.send('\r\n',0)
		
	def openSocket(self):
		#SER.send('Openning socket\r\n')
		res = MDM2.receive(1) #flush buffer
		res = MDM2.send('AT#SD=1,0,'+self.PORT_NO+',"'+self.IP+'",0\r',0)
		res = self.__expect("CONNECT",200)
		#SER.send(res+'\r\n')
		#sys.stdout = sys.stderr = GPRSWriter()
		return res
		
	def __suspend(self):
		#you must do this as follows:
		#data <wait 1sec> <send +++ without CR><wait 1sec>
		
		MOD.sleep(10)
		res = MDM2.receive(1) #flush buffer
		res = MDM2.send('+++',2)
		MOD.sleep(10)
		res = self.__expect("OK",50)
		#SER.send(res)
		
	def __resume(self):
		res = MDM2.receive(1) #flush buffer
		res = MDM2.send('AT#SO=1\r',2)
		res = self.__expect("CONNECT",50)
		#SER.send(res)
		
	def closeSocket(self):
		##SER.send('Close Socket\r\n')
		#suspend first
		self.__suspend()
		res = MDM2.receive(1) #flush buffer
		res = MDM2.send('AT#SH=1\r',2)
		res = self.__expect("OK",50)
		#SER.send(res)
		
	def __expect(self, expectedStr, timeout):
		to = 0
		res = MDM2.receive(5)
		while res.find(expectedStr)<0 and to<timeout:
			res = MDM2.receive(1)
			to = to + 1
		return res