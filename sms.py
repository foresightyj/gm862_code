import MDM
import MOD
##import SER

class Sms:
	def __init__(self):
		#print '\r\n---Starting SIM verification cycle---\r\n'
		##SER.send('Initialize SMS!\r\n')
		res = MDM.send ('AT+CPBS?\r', 0)   #Select phone book memory storage
		SIM_status = MDM.receive(10)
		if SIM_status.find("+CPBS")<0:
			##SER.send('SIM busy! Please Wait!\r\n')
			pass
		while SIM_status.find("+CPBS:")< 0 :
			res = MDM.send ('AT+CPBS?\r', 0)
			SIM_status = MDM.receive(10)
			##SER.send('Waiting for SIM ready\r\n')
			MOD.sleep(5)
		#print 'SIM ready\r\n'

		res = MDM.send('AT+CMGF=1\r', 5)	#operate in text mode
		res = MDM.receive(20)
		if res.find('OK') != -1:
			pass
		else:
			self.__init__()
		
		self.smsList = ''
		
		res = MDM.send('AT+FCLASS=8\r', 5)	#for dialing a phone no.
		res = MDM.receive(20)
		
		##SER.send('SMS Initialized.\r\n')

	def __checkTextMode(self):
		MDM.send('AT+CMGF?\r',0)
		res = MDM.receive(10)
		##SER.send(res)

	def checkNewSms(self): # check all new messages
		self.smsList = ''
		res=MDM.receive(1) #flush buffer
		MDM.send('AT+CMGL="ALL"\r',0)	#Read all new sms	
		res=''
		res=MDM.receive(10) #originally 50, but according to AT command manual, only 20 is needed in the worst case.
		self.smsList = self.smsList + res
		while res != '':
			res = MDM.receive(2)
			self.smsList = self.smsList + res
		
	def sendSms(self, to, text):
		#print 'Send SMS to: '+ to +', MSG: ' + text
		res = MDM.receive(1)#2 sec
		MDM.send('AT+CMGS="' + to + '"\r', 0)
		res = MDM.receive(1)#6 sec
		#Check for SMS prompt

		if (res == '\r\n> '):
			#Send SMS message text
			MDM.send(text, 0)

			#End SMS
			MDM.sendbyte(0x1A, 0)
			res2 = MDM.receive(60)#6 seconds
			MOD.sleep(1)#wait 0.1sec
			
			if (res2.find('\r\nOK\r\n') != -1):
				#print 'SMS sent successfully'
				status = 1
			else:
				#print 'SMS Send: ' + res2
				#print 'Did not get SMS sent confirmation'
				status = 0
		else:
			#print 'SMS Send: ' + res
			#print 'Did not receive SMS prompt'
			#Abort SMS (just in case)
			MDM.sendbyte(0x1B, 0)
			MOD.sleep(1)#wait 0.1sec
			status = 0
			
		return status

	def deleteSms(self, msg_no):
		mdmstr='at+cmgd='+msg_no+'\r'
		MDM.send(mdmstr,0)
		delreply=MDM.receive(30)
		
		if delreply.find('OK') == -1:	#Problem deleting sms
			if delreply.find('+CMS ERROR: 331') != -1: #temporary network loss
				#print '+CMS error: 331\r\n'
				MOD.sleep(10)
				self.deleteSms(msg_no)
			else:
				#print 'Problem deleting\r\n'
				return 0
		else:
			return 1
	

