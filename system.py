import MDM
import sys
import MOD
import GPIO
#import SER

from database import Database
from shell import Shell
from cc2430 import CC2430
from time import Time

DEVIDLEN = 5 # LEN of IEEE addr is 8, DEVID is the latter 5 bytes.
IEEELEN = 8
BUZZER_EN = 0

max_len = 0

def SOSBuzzer():
	if BUZZER_EN:
		GPIO.setIOvalue(7,1)
		MOD.sleep(10)
		GPIO.setIOvalue(7,0)
	
def FallBuzzer():
	if BUZZER_EN:
		cnt = 0
		while cnt < 10:
			cnt = cnt + 1
			GPIO.setIOvalue(7, cnt%2)
			MOD.sleep(1)
		GPIO.setIOvalue(7,0)

def MSG2CC2430Buzzer():
	if BUZZER_EN:
		GPIO.setIOvalue(7,1)
		MOD.sleep(1)
		GPIO.setIOvalue(7,0)
	
class System:
	def __init__(self):
		self.__initImei()
		self.database = Database() #create a Database object, will be passed to Command object.
		self.cc2430 = CC2430()
		self.shell = Shell(self) #initialize SMS before GPRS.
		self.time = Time(self)
		self.__IEEE_NWK_TABLE = {} # IEEE and NWK addresses are stored in raw byte stream in string to save processing effort.
		self.__NWK_IEEE_REVERSE_TABLE = {} # same as above
		self.__ROUTER_TABLE = {}
		self.__ROUTER_REVERSE_TABLE = {}
		self.__MSG_POOL_2_CC2430 = []
		
		self.DEBUG_EN = 1
		self.__askedForPresence = 0
		self.diffTrigger = self.database.getTrigger()
		self.LISTENER_EN = self.database.getSmsEnable()
		self.Hex2Str = self.database.Hex2Str #for faster lookup.
		self.Str2Hex = self.database.Str2Hex
		
	def __initImei(self):
		#get IMEI
		self.imei='000000000000000'
		res = MDM.receive(1) #clear buffer
		res = MDM.send('AT+CGSN\r',0)
		res = MDM.receive(10)
		timeout = 0
		
		while res.find('OK') < 0 and timeout < 100:
			res = MDM.receive(2)
			timeout = timeout + 1
		token = res.split()[0]
		
		isAllDigits = lambda str1: str1 and filter(lambda s : s >='0' and s <='9', str1) == str1
		
		if len(token) == 15 and isAllDigits(token):
			self.imei = token
		##SER.send('IMEI No: '+self.imei+'\r\n')
		
	def getImei(self):
		return self.imei

	def setDiffTrigger(self, trigger):
		self.diffTrigger = trigger
	
	def setSmsEnable(self, enable):
		self.LISTENER_EN = enable
		
	def parseCC2430Msgs(self):
		#processCC2430Msgs and processSystemEvents must be passed into the same currentTime
		global max_len
		
		CC2430Msgs, totallen = self.cc2430.receiveMsgFromCC2430()
		if totallen > max_len:
			max_len = totallen
			self.__debugstr('MAX_LEN: %d' % totallen)
		
		if totallen < 0:
			self.__debugstr('MAX_LEN: %d' % totallen)
		
		parsedCC2430Msgs = []
		try:
			for msg_id, msg_content in CC2430Msgs:
				# cc2430msg is a tuple of (MSG_ID, PAYLOAD) of type (int, string)
				
				if msg_id == self.cc2430.TEMP_REPORT_MSG_ID:	# MSG from wearble devices
					#Process raw data and return strings
					if len(msg_content) == DEVIDLEN + 2:
						num = ord(msg_content[0])
						num2 = ord(msg_content[1])
						devid = msg_content[2:]
						#devid = self.Hex2Str(devid)
						#parsedCC2430Msgs.append(devid + ':\r\nTemp: '+str(num/10)+str(num%10)+'C')
						parsedCC2430Msgs.append(('EVENT', devid, 'Temp', num*100 + num2)) #append a tuple
					else:
						#parsedCC2430Msgs.append('Temp: Error'+str(len(msg_content)))
						parsedCC2430Msgs.append(('ERROR', 'Temp', len(msg_content), self.Hex2Str(msg_content)))
						
				elif msg_id == self.cc2430.BATT_REPORT_MSG_ID: # MSG from wearble devices
					if len(msg_content) == DEVIDLEN + 1:
						num = ord(msg_content[0])
						devid = msg_content[1:]
						#devid = self.Hex2Str(devid)
						parsedCC2430Msgs.append(('EVENT', devid, 'Batt', num))
					else:
						parsedCC2430Msgs.append(('ERROR', 'Batt', len(msg_content), self.Hex2Str(msg_content)))
						
				elif msg_id == self.cc2430.SOS_ALERT_MSG_ID: # MSG from wearble devices
					if len(msg_content) == DEVIDLEN + 5:
						parent_nwk_addr = msg_content[3:5]
						devid = msg_content[5:]
						#devid = self.Hex2Str(devid)
						parsedCC2430Msgs.append(('EVENT', devid, 'Alert', msg_content[0:3], parent_nwk_addr))
					else:
						parsedCC2430Msgs.append(('ERROR', 'Alert', len(msg_content), self.Hex2Str(msg_content)))
						
				elif msg_id == self.cc2430.ACCEL_REPORT_MSG_ID: # MSG from wearble devices
					
					if len(msg_content) == DEVIDLEN + 4:
						parent_nwk_addr = msg_content[2:4]
						devid = msg_content[4:]
						#devid = self.Hex2Str(devid)
						fall = ord(msg_content[1])
						if fall == 0xF4:
							fall = 'Fall'
						elif fall == 0xF5:
							fall = 'Severe Fall'
						else:
							fall = 'Nothing'
						parsedCC2430Msgs.append(('EVENT', devid, 'Accel', fall, parent_nwk_addr))
					else:
						parsedCC2430Msgs.append(('ERROR', 'Accel', len(msg_content), self.Hex2Str(msg_content)))
						
				elif msg_id == self.cc2430.STATUS_RSP_MSG_ID: # MSG from wearble devices
					if len(msg_content) == DEVIDLEN + 5:
						temp = ord(msg_content[0])
						batt = ord(msg_content[1])
						adxl345 = ord(msg_content[2])
						parent_nwk_addr = msg_content[3:5]
						devid = msg_content[5:]
						#devid = self.Hex2Str(devid)
						
						parsedCC2430Msgs.append(('EVENT', devid, 'Summary', temp, batt, adxl345, parent_nwk_addr))
					else:
						parsedCC2430Msgs.append(('ERROR', 'Summary', len(msg_content), self.Hex2Str(msg_content)))
				
				elif msg_id == self.cc2430.IEEE_NWK_UPDATE_MSG_ID:
					if len(msg_content) == 10: # it is always of length (=2+8).
						nwk_addr = msg_content[0:2]
						ieee_addr = msg_content[2:]
						
						parsedCC2430Msgs.append(('ANNCE', (ieee_addr, nwk_addr)))						
					else:
						parsedCC2430Msgs.append(('ERROR', 'NWK_IEEE', len(msg_content), self.Hex2Str(msg_content))+ ', '+ len(msg_content))
				elif msg_id == self.cc2430.LOCATION_LINK_UPDATE_MSG_ID: #need to optimize this part
						
					if len(msg_content) == 5: 
						link_quality = msg_content[0]
						child_nwk_addr = msg_content[1:3]
						parent_nwk_addr = msg_content[3:5]
						
						parsedCC2430Msgs.append(('LOC', (child_nwk_addr, parent_nwk_addr, link_quality)))
					else:
						parsedCC2430Msgs.append(('ERROR', 'LOCATION', len(msg_content), self.Hex2Str(msg_content)))
						
				elif msg_id == self.cc2430.ROUTER_CHECK_COORD_MSG_ID: #need to optimize this part
					if len(msg_content) == 10: 
						ieee_addr = msg_content[0:8]
						nwk_addr = msg_content[8:10]
						
						parsedCC2430Msgs.append(('RTR', (ieee_addr, nwk_addr)))
					else:
						parsedCC2430Msgs.append(('ERROR', 'LOCATION', len(msg_content), self.Hex2Str(msg_content)))
				
				elif msg_id == self.cc2430.ROUTER_DELETE_CHILD_MSG_ID:
					if len(msg_content) == 4:
						child_nwk_addr = msg_content[0:2]
						parent_nwk_addr = msg_content[2:4]
						
						parsedCC2430Msgs.append(('RM', (child_nwk_addr, parent_nwk_addr)))
					else:
						parsedCC2430Msgs.append(('ERROR', 'REMOVAL', len(msg_content), self.Hex2Str(msg_content)))
				
				elif msg_id == self.cc2430.NON_CHILD_REPORT_MSG_ID:
					if len(msg_content) == 4:
						child_nwk_addr = msg_content[0:2]
						parent_nwk_addr = msg_content[2:4]
						
						parsedCC2430Msgs.append(('NONCHILD', (child_nwk_addr, parent_nwk_addr)))
					else:
						parsedCC2430Msgs.append(('ERROR', 'REMOVAL', len(msg_content), self.Hex2Str(msg_content)))
				
				elif msg_id == self.cc2430.CC2430_DEBUG_MSG_ID:
					#reversed = ''
					#for c in msg_content:
					#	reversed = c + reversed
					
					#shortaddr = ''.join(map(lambda hexv: hexv[2:], map(hex,(map(ord, reversed)))))
					#parsedCC2430Msgs.append(('DEBUG', 'DEL ' + shortaddr))
					parsedCC2430Msgs.append(('DEBUG', msg_content))
					
				elif msg_id == -1:
					#show msg_content in terms of byte values
					parsedCC2430Msgs.append(('ERROR', 'Integrity', self.Hex2Str(msg_content)))
					
				else:
					#parsedCC2430Msgs.append(('ERROR', 'Unknown', msg_content))
					parsedCC2430Msgs.append(('ERROR', 'Unknown'))

		except:
			parsedCC2430Msgs.append(('ERROR', 'Exception', self.printStackTrace()))
			#raise
			
		self.CC2430MsgBuffer =[]
		return parsedCC2430Msgs
		
	def processCC2430Msgs(self, parsedCC2430Msgs, currentTime, cache={}): 
		#processCC2430Msgs and processSystemEvents must be passed into the same currentTime
		
		# Since key is not debounced well so in one iteration, multiple key pressed can be experienced.
		# Here, before notifying listeners, we can remove duplicated messages.
		
		#remove duplicates due to debouncing.
		#msgset = []
		#for msg in parsedCC2430Msgs:
		#	if msg not in msgset:
		#		msgset.append(msg) # the order is still preserved.
		
		#made local to expedite lookup.
		replaceByIEEEAlias = self.database.replaceByIEEEAlias
		
		getIEEEAndPhonesByDevId = self.database.getIEEEAndPhonesByDevId
		replaceByPhoneAlias = self.database.replaceByPhoneAlias
		getIEEEAlias = self.database.getIEEEAlias
		
		alertsAlreadySent =[] #as a means to remove duplicated alerts.
		for parsedCC2430Msg in parsedCC2430Msgs:
			if parsedCC2430Msg[0] == 'EVENT':
				devid = parsedCC2430Msg[1]
				ieeeandphonenos = getIEEEAndPhonesByDevId(devid)
				
				if ieeeandphonenos:
					msg_type = parsedCC2430Msg[2]
					devicealias = getIEEEAlias(ieeeandphonenos[0])
					if not devicealias:
						devicealias = devid #not ieee, just to save bytes in SMS.
					#notification = devicealias + ': ' + msg_type + ': '
					notification = devicealias + ': ' #don't show msg_type, to save bytes on data.
					#self.__debugstr('msg_type:'+msg_type)
					if msg_type == 'Temp':
						decimalpart = str(parsedCC2430Msg[3]%100)
						if len(decimalpart) == 1:
							decimalpart = '0'+decimalpart
						notification = notification + str(parsedCC2430Msg[3]/100)+'.'+decimalpart+'C'
					elif msg_type == 'Batt':
						notification = notification + str(parsedCC2430Msg[3]/10)+'.'+str(parsedCC2430Msg[3]%10)+'V'	
					elif msg_type == 'Alert':
						#SOSBuzzer()
						notification = notification + parsedCC2430Msg[3] + ', near ' + self.identifyRouterName(parsedCC2430Msg[4])
					elif msg_type == 'Accel':
						#FallBuzzer()
						notification = notification + str(parsedCC2430Msg[3]) + ', near ' + self.identifyRouterName(parsedCC2430Msg[4])
					elif msg_type == 'Summary':
						notification = notification +str(parsedCC2430Msg[3]/10)+str(parsedCC2430Msg[3]%10)+'C,'+str(parsedCC2430Msg[4]/10)+'.'+str(parsedCC2430Msg[4]%10)+'V,'+hex(parsedCC2430Msg[5])[-2:].upper() \
						+ ', near ' + self.identifyRouterName(parsedCC2430Msg[6])
					else:
						pass
					
					if msg_type in ('Accel', 'Alert', 'Summary'):
						caregivers = ieeeandphonenos[1]
						caregivers = map(replaceByPhoneAlias, caregivers)
						
						if (msg_type, devicealias) not in alertsAlreadySent:
							alertsAlreadySent.append((msg_type, devicealias)) #just record msg_type and devicealias, ignore other 
							self.__debugstr('Notify ' + ', '.join(caregivers) + ': ' + notification)
							if self.LISTENER_EN:
								for phoneno in ieeeandphonenos[1]:
									MOD.sleep(10) #sleep for 1 second. sending 2 consecutive msgs without intervals could cause problems.
									self.shell.sendMsg(phoneno, notification)
						else:
							#self.__debugstr('Didn\'t notify ' + ', '.join(caregivers) + ': ' + notification)
							pass
							
			elif parsedCC2430Msg[0] == 'ANNCE':
				#update the NWK_IEEE table about the change
				ieeeHex, nwk = parsedCC2430Msg[1]

				if self.__IEEE_NWK_TABLE.has_key(ieeeHex):
					#check its previous timestamp. If 0, it means this device previously missing.
					if self.__IEEE_NWK_TABLE[ieeeHex][1] < 1000:
						self.__debugstr('%s is back (%s).' % (replaceByIEEEAlias(ieeeHex), self.database.Hex2Str(nwk)))
					
					if self.__IEEE_NWK_TABLE[ieeeHex][0] == nwk: #just update the time
						if self.DEBUG_EN and self.__IEEE_NWK_TABLE[ieeeHex][1] >= 1000:
							#this could happen quite a lot, it seems zigbee broadcast duplicated end_device_annce to make sure it gets through. Never mind, just display all.
							# stays the same.
							self.__debugstr('%s(%s).' % (replaceByIEEEAlias(ieeeHex), self.database.Hex2Str(nwk)))
						
						self.__IEEE_NWK_TABLE[ieeeHex][1] = currentTime
					else: # WD changed parent
						#make quick reverse NWK->IEEE access table for LOCATION UPDATE
						#delete the old nwk addr
						if self.__IEEE_NWK_TABLE[ieeeHex][1] >= 1000:
							self.__debugstr(replaceByIEEEAlias(ieeeHex) + ':%s->%s' % (self.database.Hex2Str(self.__IEEE_NWK_TABLE[ieeeHex][0]), self.database.Hex2Str(nwk)))
						
						if self.__NWK_IEEE_REVERSE_TABLE.has_key(self.__IEEE_NWK_TABLE[ieeeHex][0]):
							del self.__NWK_IEEE_REVERSE_TABLE[self.__IEEE_NWK_TABLE[ieeeHex][0]]
						#and add in the new nwk addr
						if self.__NWK_IEEE_REVERSE_TABLE.has_key(nwk):
							self.__NWK_IEEE_REVERSE_TABLE[nwk][0] = ieeeHex
						else:
							self.__NWK_IEEE_REVERSE_TABLE[nwk] = [ieeeHex, None]
						
						#update the IEEE->NWK table
						self.__IEEE_NWK_TABLE[ieeeHex] = [nwk, currentTime]
				else:
					#First announcement of this IEEE addr.
					self.__debugstr( '%s joined(%s).' % (replaceByIEEEAlias(ieeeHex,cachethis=1), self.database.Hex2Str(nwk)))
					self.__IEEE_NWK_TABLE[ieeeHex] = [nwk, currentTime]
					self.__NWK_IEEE_REVERSE_TABLE[nwk] = [ieeeHex, None] #no parent info from annce so place None here.
					
			elif parsedCC2430Msg[0] == 'LOC':
				child_nwk, parent_nwk, lqi = parsedCC2430Msg[1]
				if self.__NWK_IEEE_REVERSE_TABLE.has_key(child_nwk):
					#update parent address
					self.__NWK_IEEE_REVERSE_TABLE[child_nwk][1]=parent_nwk
					#This nwk addr has been announced at least once so the system has records
					#Also update its time frame
					ieeeHex = self.__NWK_IEEE_REVERSE_TABLE[child_nwk][0]
					
					if self.__IEEE_NWK_TABLE[ieeeHex][1] < 1000:
						self.__debugstr("%s didn't leave (%s)." % (replaceByIEEEAlias(ieeeHex), self.database.Hex2Str(child_nwk)))
					
					self.__IEEE_NWK_TABLE[ieeeHex][1] = currentTime
				else:
					#Unknown device updating its location to the system
					#Need to query for its IEEE address.
					if not cache.has_key(child_nwk):
						cache[child_nwk] = 0
					else:
						cache[child_nwk] = cache[child_nwk] + 1
					
					if cache[child_nwk] in (0,2,5,10) or cache[child_nwk] % 40 == 0: # only pools for five times.
						#self.add2MsgPool(self.cc2430.IEEE_ADDR_REQ_MSG_ID, '', child_nwk) # '' in the ieeeHex field means sending to coordinator
						self.add2MsgPool(self.cc2430.WHAT_IS_YOUR_IEEE_MSG_ID, '', child_nwk)
						
						if self.DEBUG_EN:
							self.__debugstr('$' + self.database.Hex2Str(child_nwk))
					elif cache[child_nwk] < 20 and cache[child_nwk] % 4 == 1:
						if self.DEBUG_EN:
							self.__debugstr('!' + self.database.Hex2Str(child_nwk))
						
			elif parsedCC2430Msg[0] == 'RTR':
				ieeeHex, nwkHex = parsedCC2430Msg[1]
				if not self.__ROUTER_TABLE.has_key(ieeeHex):
					#first time appeared in the system
					#occassionally, router reports rubbish ieeeHex right after reboot.
					
					#three lines of code below makes it unstable thus temporarily removed.
					if self.__ROUTER_REVERSE_TABLE.has_key(nwkHex):
						if self.__ROUTER_TABLE.has_key(self.__ROUTER_REVERSE_TABLE[nwkHex]):
							temp =self.__ROUTER_REVERSE_TABLE[nwkHex]
							if self.__ROUTER_REVERSE_TABLE.has_key(self.__ROUTER_TABLE[self.__ROUTER_REVERSE_TABLE[nwkHex]][0]):
									del self.__ROUTER_REVERSE_TABLE[self.__ROUTER_TABLE[self.__ROUTER_REVERSE_TABLE[nwkHex]][0]]
							del self.__ROUTER_TABLE[temp]
					self.__ROUTER_TABLE[ieeeHex] = [nwkHex, currentTime]
					self.__ROUTER_REVERSE_TABLE[nwkHex] = ieeeHex

					if self.DEBUG_EN:
						self.__debugstr('RTR:' + self.database.replaceRouterAlias(ieeeHex))
				else:
					#just update the time stamp
					if self.__ROUTER_TABLE[ieeeHex][0] == nwkHex:
						if self.__ROUTER_TABLE[ieeeHex][0] < 1000 and DEBUG_EN: #previously disappeared
							__debugstr('RTR %s is back' % database.replaceRouterAlias(ieeeHex))
						self.__ROUTER_TABLE[ieeeHex][1] = currentTime
						
					else:
						#ROUTER changed short address
						if self.DEBUG_EN:
							self.__debugstr('RTR %s: %s->%s' % (self.database.replaceRouterAlias(ieeeHex), self.database.Hex2Str(self.__ROUTER_TABLE[ieeeHex][0]), self.database.Hex2Str(nwkHex)))
						if self.__ROUTER_REVERSE_TABLE.has_key(self.__ROUTER_TABLE[ieeeHex][0]):
								temp = self.__ROUTER_TABLE[ieeeHex][0]
								if self.__ROUTER_TABLE.has_key(self.__ROUTER_REVERSE_TABLE[self.__ROUTER_TABLE[ieeeHex][0]]):
										del self.__ROUTER_TABLE[self.__ROUTER_REVERSE_TABLE[self.__ROUTER_TABLE[ieeeHex][0]]]
								del self.__ROUTER_REVERSE_TABLE[temp]
						if self.__ROUTER_REVERSE_TABLE.has_key(nwkHex):
								if self.__ROUTER_TABLE.has_key(self.__ROUTER_REVERSE_TABLE[nwkHex]):
										del self.__ROUTER_TABLE[self.__ROUTER_REVERSE_TABLE[nwkHex]]
								del self.__ROUTER_REVERSE_TABLE[nwkHex]
						
						self.__ROUTER_TABLE[ieeeHex] = [nwkHex, currentTime]
						self.__ROUTER_REVERSE_TABLE[nwkHex] = ieeeHex						
				
			elif parsedCC2430Msg[0] == 'RM':
				child = parsedCC2430Msg[1][0]
				parent = parsedCC2430Msg[1][1]
				if self.DEBUG_EN:
					self.__debugstr('%s rm %s.' % (self.database.Hex2Str(parent), self.database.Hex2Str(child)))
			
			elif parsedCC2430Msg[0] == 'NONCHILD':
				child = parsedCC2430Msg[1][0]
				parent = parsedCC2430Msg[1][1]
				if self.DEBUG_EN:
					self.__debugstr('Non-child %s->%s.' % (self.database.Hex2Str(child), self.database.Hex2Str(parent)))
			
			elif parsedCC2430Msg[0] == 'ERROR':
				self.__debugstr('?'+str(parsedCC2430Msg))
			elif parsedCC2430Msg[0] == 'DEBUG' and self.DEBUG_EN:
				self.__debugstr('??'+str(parsedCC2430Msg))
			elif self.DEBUG_EN:
				self.__debugstr('???'+str(parsedCC2430Msg))
				
	def add2MsgPool(self, msg_id, ieeeHex, payload=''):
		self.__MSG_POOL_2_CC2430.append([msg_id, ieeeHex, payload, 0]) # 0 is the retrycount
		
	def processSystemEvents(self, currentTime, checkedtime={}):
		# ieeeHex can be : 1) ieee addr, 2) '', which means sending to CC2430 coordinator
		REMINDERLIMIT = 5
		QUERYSTATUSLIMIT = 5
		NWKADDRREQLIMIT = 2 # This one is sent as broadcast, so send less at a time.
		
		rlcount = 0
		qslcount = 0
		narlcount = 0
		try:
			# 1) Check if all system devices are present
			# Only done ONCE per boot
			#check if all authorized devices are in the self.__IEEE_NWK_TABLE.
			if self.__askedForPresence == 5:
				self.__askedForPresence = self.__askedForPresence + 1
				allDevicesInSystem = self.database.devicelist.keys()
				
				if len(allDevicesInSystem) == len(self.__IEEE_NWK_TABLE):
					#everything is OK.
					pass
				else:
					#some devices in the system are not present in the network. Must check them.
					
					for ieeeHex in allDevicesInSystem:
						if not self.__IEEE_NWK_TABLE.has_key(ieeeHex):
							self.__debugstr('Seeking ' + self.database.replaceByIEEEAlias(ieeeHex))
							self.add2MsgPool(self.cc2430.NWK_ADDR_REQ_MSG_ID, ieeeHex)
				
			elif self.__askedForPresence < 5:
				# Right after reboot, we don't check for presence immediately but wait for 5 cycles.
				self.__askedForPresence = self.__askedForPresence + 1
			
			# 2) check if any existing router is updating its status once per minute
				#check every five minutes
			this_interval = currentTime/60
			if checkedtime.get('everymin', 0) != this_interval: # this means there hasn't been anything done in this 6 minutes
				checkedtime['everymin'] = this_interval
				for ieeeHex, nwkHex_count in self.__ROUTER_TABLE.items():
					if currentTime - nwkHex_count[1] > 60 and nwkHex_count[1]>1000:
						#report it
						if self.DEBUG_EN:
							self.__debugstr('RTR %s disappeared' % self.database.replaceRouterAlias(ieeeHex))
							del self.__ROUTER_TABLE[ieeeHex]
						#don't remove it, instead mark its timestamp as obsolete.
						nwkHex_count[1] = 0
						
				#2.5) also take this chance to check balance.
				this_interval, this_time = divmod(currentTime, 86400)
				if checkedtime.get('everyday', 0) != this_interval and this_time > ((1+12)*60+40)*60: #1:40pm
					checkedtime['everyday'] = this_interval
					self.checkBalance()
					
			# 3) Check if any device is not reporting to BS for quite a while
			for ieeeHex, nwk_and_time in self.__IEEE_NWK_TABLE.items():
				nwk, time = nwk_and_time
				#need to take time of each system cycle into account.
				diff = currentTime - time
				
				#if diff > 8 and time > 1000:
				#	#send out nwk_addr_req
				#	self.add2MsgPool(self.cc2430.NWK_ADDR_REQ_MSG_ID, '', ieeeHex)
					
				if diff > self.diffTrigger and time > 1000: #if more than 10 seconds, consider it missing.
					#Alert caregiver about the absence of this ieeeHex.
					caregivers = self.database.getCaregiversByIEEE(ieeeHex)
					parent_nwk = self.__NWK_IEEE_REVERSE_TABLE.get(nwk,('',''))[1]
					loc_info = ''
					if parent_nwk != None:
						if parent_nwk == '':
							self.__debugstr('^:%s' % self.database.Hex2Str(nwk))
							#self.__debugstr(str(self.__IEEE_NWK_TABLE))
							#self.__debugstr(str(self.__NWK_IEEE_REVERSE_TABLE))
							# Exam below when I have free time.
							# When conflict occurs, we have the following states of
							# __IEEE_NWK_TABLE:
							# 2726252423220004 7971
							# 2726252423220005 7973
							# 2726252423220006 178e
							# 2726252423220007 7972
							# 2726252423220009 7973
							# 2726252423220003 796f
							# __NWK_IEEE_REVERSE_TABLE:
							# 796f 2726252423220003
							# 7972 2726252423220007
							# 7973 2726252423220005
							# 178e 2726252423220006
						else:
							loc_info = '@ ' + self.identifyRouterName(parent_nwk)
					notification = self.database.replaceByIEEEAlias(ieeeHex) + ' left(%d) %s.' % (diff, loc_info)
					
					nwk_and_time[1] = 0 #nwk_and_time holds a reference so this is an inplace change.
										#set the timestamp to zero so it won't be alerting next time (before it re-appeares)
					self.__debugstr(', '.join(map(self.database.replaceByPhoneAlias, caregivers)) + '! ' + notification)
					
					if self.LISTENER_EN:
						for phoneno in caregivers:
							self.shell.sendMsg(phoneno, notification)
							MOD.sleep(10) #pause for a while.
				
					
				
			# 4) Process Reminders
			ieeelist = self.time.checkReminder(currentTime)
			#self.__debugstr('Time: ' + str(self.time.getTime()))
			#self.__debugstr('Reminders: ' + str(ieeelist))
			for ieeeHex in ieeelist:
				#add to msg pool, let system alert them at appropriate time (to avoid overloading the zigbee nwk)
				if self.isOnline(ieeeHex):
					self.__debugstr('Reminder sent to ' + self.database.replaceByIEEEAlias(ieeeHex))
					self.add2MsgPool(self.cc2430.REMINDER_MSG_ID, ieeeHex)
				else:
					self.__debugstr('Reminder failure: %s offline' % self.database.replaceByIEEEAlias(ieeeHex))
					
			# 5) Process Message Pools
			toberemoved = []
			for msg in self.__MSG_POOL_2_CC2430:
				# msg is a tuple of (msg_id, ieeeHex, payload)
				msg_id, ieeeHex, payload, nwkAddrReqRetryCnt = msg
				
				if ieeeHex: # ieeeHex are checked against the database before being passed to this func.
					# resolve ieee into nwk
					nwk = self.getActiveNwk(ieeeHex)
					
					if nwk:
						if msg_id == self.cc2430.REMINDER_MSG_ID:
							rlcount = rlcount + 1
							#rlcount = rlcount + GPIO.getADC(1)%2 # add either 0 or 1 randomly.
							if rlcount > REMINDERLIMIT:
								continue
						elif msg_id  == self.cc2430.STATUS_REQ_MSG_ID:
							qslcount = qslcount + 1
							#qslcount = qslcount + GPIO.getADC(1)%2 # add either 0 or 1 randomly.
							if qslcount > QUERYSTATUSLIMIT:
								continue
						elif msg_id == self.cc2430.NWK_ADDR_REQ_MSG_ID:
							narlcount = narlcount + 1
							#narlcount = narlcount + GPIO.getADC(1)%2 # add either 0 or 1 randomly.
							if narlcount > NWKADDRREQLIMIT:
								continue
						
						# send it out
						
						self.cc2430.sendMsg2CC2430(msg_id, payload+nwk) #exception here
						#self.__debugstr('Processed: ' + str(msg) + ', NWK: ' + nwk)
						#MSG2CC2430Buzzer()
						# delete it from the msg pool
						toberemoved.append(msg)
						
					else:
						# __IEEE_NWK_TABLE does not have the record of NWK for this ieee ieeeHex.
						# invoke NWK_ADDR_REQ in CC2430
						
						if nwkAddrReqRetryCnt % 5 == 0: # only send the first time, the sixth time, etc.
							#self.__debugstr('Looking for ' + self.database.Hex2Str(ieeeHex))
							self.cc2430.sendMsg2CC2430(self.cc2430.NWK_ADDR_REQ_MSG_ID, ieeeHex)
							
							# update the retry count
							msg[3] = nwkAddrReqRetryCnt + 1
							
							# system will come back later to check if NWK is updated by NWK_ADDR_REQ
							# if yes, will resend this msg.
						
						elif nwkAddrReqRetryCnt > 5:
							# should stop retrying
							
							# issue a message back to querer
							# for now, just send a debug message
							self.__debugstr("Failed to find " + self.database.replaceByIEEEAlias(msg[1]))
							
							# delete the entry
							toberemoved.append(msg)
							
						else:
							msg[3] = nwkAddrReqRetryCnt + 1
							
				else: # '', send to CC2430 coordinator
					self.cc2430.sendMsg2CC2430(msg_id, payload)
					# delete it from the msg pool
					toberemoved.append(msg)
					
				# apply some pause when sending to CC2430
				MOD.sleep(2)
			
			#END of for-looping all messages in the pool
			
			#Now remove msgs in toberemoved
			for msg in toberemoved:
				self.__MSG_POOL_2_CC2430.remove(msg)
		
		except:
			self.__debugstr(self.printStackTrace('System Exception:\r\n'))
	
	def checkBalance(self):
		MDM.send('ATD *1344\r',2)
		res = MDM.receive(20)
		timeout = 0
		while(res.find('OK')<0 and timeout <5):
			timeout = timeout + 1
			res = res + MDM.receive(10)
			if res.find('NO CARRIER')>-1:
				break
		reply_msg = res
		
		MDM.send('ATH\r',2)
		res = MDM.receive(20)
		timeout = 0
		while(res.find('OK')<0 and timeout <5):
			timeout = timeout + 1
			res = res + MDM.receive(10)
		
		reply_msg = reply_msg + res
		return reply_msg
		
	def isOnline(self,ieeeHex):
		#time field must be good.
		return self.__IEEE_NWK_TABLE.get(ieeeHex,('',0))[1] >= 1000
		
	def getActiveStatus(self):
		#'''
		#This function returns something like ('old3', 0x796f, 1) which means old3 which has nwk addr of 0x796f is online.
		#'''
		return map(lambda ieee_nwktime, replace=self.database.replaceByIEEEAlias: (replace(ieee_nwktime[0]), ieee_nwktime[1][0], ieee_nwktime[1][1]>=1000), self.__IEEE_NWK_TABLE.items())
	
	def getRouterStatus(self):
		return map(lambda ieee_nwktime, replace=self.database.replaceRouterAlias: (replace(ieee_nwktime[0]), ieee_nwktime[1][0], ieee_nwktime[1][1]>=1000), self.__ROUTER_TABLE.items())
	
	def identifyRouterName(self, nwkHex):
		#'''
		#If found corresponding IEEE address, return its alias if possible
		#'''
		if nwkHex == chr(0x00)+chr(0x00):
			return 'BASE'

		ieeeHex = self.__ROUTER_REVERSE_TABLE.get(nwkHex, '')
		
		if ieeeHex:
			return self.database.replaceRouterAlias(ieeeHex)
		else:
			return self.database.Hex2Str(nwkHex)
		
	def getActiveNwk(self, ieee):
		# get NWK using IEEE from the __IEEE_NWK_TABLE
		#if self.__IEEE_NWK_TABLE.has_key(ieee):
		#	return self.__IEEE_NWK_TABLE[ieee][0]
		#
		#return ''
		nwkHex, timestamp = self.__IEEE_NWK_TABLE.get(ieee,('', 0)) #more efficient
		if timestamp > 1000:
			return nwkHex
		else:
			return ''
		
	def __debugstr(self, msg):
		if self.shell.socket:
			self.shell.socket.sendGprsMsg(msg)

	def printStackTrace(self, msg = ''):
		reply_msg = msg
		exc_type, exc_value, exc_traceback = sys.exc_info()
		reply_msg = reply_msg + '1) '+ str(exc_type)+'\r\n'
		reply_msg = reply_msg + '2) '+ str(exc_value)+'\r\n'
		#reply_msg = reply_msg + '3) file:'+ str(exc_traceback.tb_frame.f_code)+'\r\n'
		#reply_msg = reply_msg + '4) line:'+ str(exc_traceback.tb_lineno)+'\r\n'
		return reply_msg
		
# End of Class GpsTracker

